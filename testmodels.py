import numpy as np
from typing import List, Dict

def straight_cantilever_mcneal_qd1(usetransf:bool)->(List[np.ndarray],Dict[int,List[int]],List[float],List[List],List[List],str,str):
    numel = 6
    dxshiftratio = 0.0
    outputname = 'straight_cantilever'
    description ='Standard tests: straight cantilever, ' + str(numel)
    #if dxshiftratio != 0.0:
     #   description += ' rectangulatr elements'
    #else:
     #   description += ' trapezoidal elements ( ' + str (dxshiftratio)+' x shift to element length ratio)'
    beam_length = 6.0
    dx = beam_length / numel
    ddx = dx * dxshiftratio  # use for generating nonrectangular (trapezoidal) elements
    dy = 0.2
    y = dy
    model_nodes = []
    elnodeindices = {}
    # add first nodes
    model_nodes.append(np.array([0, 0.0, 0.0]))
    model_nodes.append(np.array([0, y, 0.0]))
    # add nodes
    for i in range(0, numel - 1):
        x = (i + 1) * dx
        model_nodes.append(np.array([x + ddx, 0.0, 0.0]))
        model_nodes.append(np.array([x - ddx, y, 0.0]))
        iln = len(model_nodes) - 1
        elnodeindices[i+1] = [iln - 3, iln - 1, iln, iln - 2]
    # add last nodes
    x = numel * dx
    model_nodes.append(np.array([x, 0.0, 0.0]))
    model_nodes.append(np.array([x, y, 0.0]))
    iln = len(model_nodes) - 1
    elnodeindices[numel] = [iln - 3, iln - 1, iln, iln - 2]

    eldata = [0.0] * 3
    eldata[0] = 10000000  # E
    eldata[1] = 0.3  # ni
    eldata[2] = 0.1  # tp
    # Boundary conditions
    bcdata = []
    #             [inod,dof,indices]
    bcdata.append([1, 1, 0.0])
    bcdata.append([1, 2, 0.0])
    bcdata.append([2, 1, 0.0])
    bcdata.append([2, 2, 0.0])

    # Load
    fdata = []
    #             [inod,dof,indices]
    fdata.append([iln, 2, 0.5])  # Apply load on last two nodes
    fdata.append([iln+1, 2, 0.5])  #
    return model_nodes,elnodeindices,eldata,bcdata,fdata,outputname,description

def straight_cantilever_mcneal_knjiga(usetransf:bool)->(List[np.ndarray],Dict[int,List[int]],List[float],List[List],List[List],str,str):
    numel = 4
    dxshiftratio = 0.0
    outputname = 'mcneal_knjiga'
    description ='McNeal knjiga, ' + str(numel)
    if dxshiftratio != 0.0:
        description += ' rectangulatr elements'
    else:
        description += ' trapezoidal elements ( ' + str (dxshiftratio)+' x shift to element length ratio)'
    beam_length = 48.0
    dx = beam_length / numel
    ddx = dx * dxshiftratio  # use for generating nonrectangular (trapezoidal) elements
    dy = 12
    y = dy
    model_nodes = []
    elnodeindices = {}
    # add first nodes
    model_nodes.append(np.array([0, 0.0, 0.0]))
    model_nodes.append(np.array([0, y, 0.0]))
    # add nodes
    for i in range(0, numel - 1):
        x = (i + 1) * dx
        model_nodes.append(np.array([x + ddx, 0.0, 0.0]))
        model_nodes.append(np.array([x - ddx, y, 0.0]))
        iln = len(model_nodes) - 1
        elnodeindices[i+1] = [iln - 3, iln - 1, iln, iln - 2]
    # add last nodes
    x = numel * dx
    model_nodes.append(np.array([x, 0.0, 0.0]))
    model_nodes.append(np.array([x, y, 0.0]))
    iln = len(model_nodes) - 1
    elnodeindices[numel] = [iln - 3, iln - 1, iln, iln - 2]

    eldata = [0.0] * 3
    eldata[0] = 10000000  # E
    eldata[1] = 0.3  # ni
    eldata[2] = 0.1  # tp
    # Boundary conditions
    bcdata = []
    #             [inod,dof,indices]
    bcdata.append([1, 1, 0.0])
    bcdata.append([1, 2, 0.0])
    bcdata.append([2, 1, 0.0])
    bcdata.append([2, 2, 0.0])

    # Load
    fdata = []
    #             [inod,dof,indices]
    fdata.append([iln, 2, 50])  # Apply load on last two nodes
    fdata.append([iln + 1, 2, 50])  #
    return model_nodes,elnodeindices,eldata,bcdata,fdata,outputname,description

def straight_cantilever_mcneal_qd2rot(usetransf:bool)->(List[np.ndarray],Dict[int,List[int]],List[float],List[List],List[List],str,str):
    numel = 6
    dxshiftratio = 0.0
    outputname = 'straight_cantilever'
    description ='Standard tests: straight cantilever, ' + str(numel)
    if dxshiftratio != 0.0:
        description += ' rectangulatr elements'
    else:
        description += ' trapezoidal elements ( ' + str (dxshiftratio)+' x shift to element length ratio)'
    beam_length = 6.0
    dx = beam_length / numel
    ddx = dx * dxshiftratio  # use for generating nonrectangular (trapezoidal) elements
    dy = 0.2
    y = dy
    model_nodes = []
    elnodeindices = {}
    # add first nodes
    model_nodes.append(np.array([0, 0.0, 0.0]))
    model_nodes.append(np.array([0, y, 0.0]))
    # add nodes
    for i in range(0, numel - 1):
        x = (i + 1) * dx
        model_nodes.append(np.array([x + ddx, 0.0, 0.0]))
        model_nodes.append(np.array([x - ddx, y, 0.0]))
        iln = len(model_nodes) - 1
        elnodeindices[i+1] = [iln - 3, iln - 1, iln, iln - 2]
    # add last nodes
    x = numel * dx
    model_nodes.append(np.array([x, 0.0, 0.0]))
    model_nodes.append(np.array([x, y, 0.0]))
    iln = len(model_nodes) - 1
    elnodeindices[numel] = [iln - 3, iln - 1, iln, iln - 2]

    eldata = [0.0] * 3
    eldata[0] = 10000000  # E
    eldata[1] = 0.3  # ni
    eldata[2] = 0.1  # tp
    # Boundary conditions
    bcdata = []
    #             [inod,dof,indices]
    bcdata.append([1, 1, 0.0])
    bcdata.append([1, 2, 0.0])
    bcdata.append([1, 3, 0.0])
    bcdata.append([2, 1, 0.0])
    bcdata.append([2, 2, 0.0])
    bcdata.append([2, 3, 0.0])


    # Load
    fdata = []
    #             [inod,dof,indices]
    fdata.append([iln, 2, 0.5])  # Apply load on last two nodes
    fdata.append([iln+1, 2, 0.5])  #
    return model_nodes,elnodeindices,eldata,bcdata,fdata,outputname,description

def first(usetransf:bool)->(List[np.ndarray],Dict[int,List[int]],List[float],List[List],List[List],str,str):
    numel = 1
    dxshiftratio = 0.0
    outputname = 'first'
    description ='first, ' + str(numel)
    if dxshiftratio != 0.0:
        description += ' rectangulatr elements'
    else:
        description += ' trapezoidal elements ( ' + str (dxshiftratio)+' x shift to element length ratio)'
    x = 2
    y = 2
    model_nodes = []
    elnodeindices = {}
    # add first nodes
    model_nodes.append(np.array([0.0, 0.0, 0.0]))
    model_nodes.append(np.array([x, 0.0, 0.0])) 
    model_nodes.append(np.array([x, y, 0.0])) 
    model_nodes.append(np.array([0.0, y, 0.0]))

    elnodeindices[1] = [0, 1, 2, 3]

    eldata = [0.0] * 3
    eldata[0] = 10000000  # E
    eldata[1] = 0.3  # ni
    eldata[2] = 0.1  # tp
    # Boundary conditions
    bcdata = []
    #             [inod,dof,indices]
    bcdata.append([1, 1, 0.0])
    bcdata.append([1, 2, 0.0])
    bcdata.append([4, 1, 0.0])
    bcdata.append([4, 2, 0.0])

    # Load
    fdata = []
    #             [inod,dof,indices]
    fdata.append([2, 1, 1.0])  # Apply load on last two nodes
    fdata.append([3, 1, 1.0])  #
    return model_nodes,elnodeindices,eldata,bcdata,fdata,outputname,description
