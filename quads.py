# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 18:06:26 2021

@author: mpalaversa
"""

# This file comprises following quadrialteral plane stress "membrane" elements:
# PlnStrssQd1, PlnStrssQd1Red1, PlStrssQd1Red2, PlStrssQd2, PlStrssQd1Rot2 and
# PlnStrssQd1Rot3.

# DEPENDENCIES
import sys
sys.path.insert(1, r'C:\Users\mpalaversa\OneDrive - Fakultet strojarstva i brodogradnje\Dokumenti\Predlošci')

import datetime as dt
import latexout as lo
import sympy as sp

print(dt.datetime.now().strftime('%X') + '\t' + 'Starting...')
sp.init_printing()
latexOut = lo.LatexOut('quads', 'article', 'a3', 'landscape', '12', 'english', 'My Quads', 'Marin Palaversa')


latexOut.addSection('Common variables')
latexOut.addText('Material characteristics Young\'s modulus and Poissons coefficient:')
Ee, nu = sp.symbols(r"E,\nu")
latexOut.addMath(sp.latex(Ee))
latexOut.addMath(sp.latex(nu))

latexOut.addText('Metric and parametric coordinates:')
x, y, xi, eta = sp.symbols(r"x,y,\xi,\eta")
latexOut.addMath(sp.latex(x))
latexOut.addMath(sp.latex(y))
latexOut.addMath(sp.latex(xi))
latexOut.addMath(sp.latex(eta))

latexOut.addText('Thickness:')
t = sp.symbols("t")
latexOut.addMath(sp.latex(t))

latexOut.addText('Constitutive matrix (based on plane stress assumption of 2D elasticity):')
D = (Ee/(1-nu**2))*sp.Matrix([[1,nu,0],[nu,1,0],[0,0,(1-nu)/2]])
latexOut.addMath(sp.latex(D))


latexOut.addSection('4-node plane stress quadrilateral (PlStrssQd1)')

latexOut.addSubsection('Position/displacement basis function (based on isoparametric FE assumption)')
X4_1j = sp.Matrix([[1,xi,eta,xi*eta]])
latexOut.addMath(sp.latex(X4_1j))

latexOut.addSubsection('Nodes\' position')
latexOut.addText('In parametric space:')
xi1, eta1, xi2, eta2, xi3, eta3, xi4, eta4 = sp.symbols(r"\xi_1,\eta_1,\xi_2,\eta_2,\xi_3,\eta_3,\xi_4,\eta_4")
latexOut.addMath(sp.latex(xi1))
latexOut.addMath(sp.latex(eta1))
latexOut.addMath(sp.latex(xi2))
latexOut.addMath(sp.latex(eta2))
latexOut.addMath(sp.latex(xi3))
latexOut.addMath(sp.latex(eta3))
latexOut.addMath(sp.latex(xi4))
latexOut.addMath(sp.latex(eta4))
latexOut.addText('In metric space:')
x1, y1, x2, y2, x3, y3, x4, y4 = sp.symbols("x_1,y_1,x_2,y_2,x_3,y_3,x_4,y_4")
latexOut.addMath(sp.latex(x1))
latexOut.addMath(sp.latex(y1))
latexOut.addMath(sp.latex(x2))
latexOut.addMath(sp.latex(y2))
latexOut.addMath(sp.latex(x3))
latexOut.addMath(sp.latex(y3))
latexOut.addMath(sp.latex(x4))
latexOut.addMath(sp.latex(y4))

latexOut.addSubsection('Vector of nodal coordinates (in metric space):')
x4_i = sp.Matrix([[x1],[y1],[x2],[y2],[x3],[y3],[x4],[y4]])
latexOut.addMath(sp.latex(x4_i))

latexOut.addSubsection('Matrix of constants:')
X4_ij = sp.Matrix([X4_1j.subs([(xi,xi1),(eta,eta1)]),X4_1j.subs([(xi,xi2),(eta,eta2)]),\
                   X4_1j.subs([(xi,xi3),(eta,eta3)]),X4_1j.subs([(xi,xi4),(eta,eta4)])])
latexOut.addMath(sp.latex(X4_ij))

latexOut.addSubsection('Displacement shape functions:')
N4_1i = X4_1j*(X4_ij.subs([(xi1,-1),(eta1,-1),(xi2,-1),(eta2,1),(xi3,1),(eta3,-1),(xi4,1),(eta4,1)]))**-1
latexOut.addMath(sp.latex(N4_1i))

latexOut.addSubsection('Jacobian matrix, its inverse and determinant')
print(dt.datetime.now().strftime('%X') + '\t' + 'Calculating Jacobian.')
latexOut.addText('Derivatives of shape functions by parametric coordinates:')
dN_dxi = sp.diff(N4_1i, xi)
dN_deta = sp.diff(N4_1i, eta)
latexOut.addMath(sp.latex(dN_dxi))
latexOut.addMath(sp.latex(dN_deta))
latexOut.addText('Derivatives of metric by parametric coordinates:')
dx_dxi = dN_dxi * sp.Matrix([x4_i.row(0), x4_i.row(2), x4_i.row(4), x4_i.row(6)])
dy_dxi = dN_dxi * sp.Matrix([x4_i.row(1), x4_i.row(3), x4_i.row(5), x4_i.row(7)])
dx_deta = dN_deta * sp.Matrix([x4_i.row(0), x4_i.row(2), x4_i.row(4), x4_i.row(6)])
dy_deta = dN_deta * sp.Matrix([x4_i.row(1), x4_i.row(3), x4_i.row(5), x4_i.row(7)])
latexOut.addMath(sp.latex(dx_dxi))
latexOut.addMath(sp.latex(dy_dxi))
latexOut.addMath(sp.latex(dx_deta))
latexOut.addMath(sp.latex(dy_deta))
latexOut.addText('Jacobian matrix:')
jac = sp.Matrix([[dx_dxi, dy_dxi],[dx_deta, dy_deta]])
latexOut.addMath(sp.latex(jac))
latexOut.addText('Inverse of the Jacobian matrix:')
jac_inv = jac**-1
latexOut.addMath(sp.latex(jac_inv))
latexOut.addText('Determinant of the Jacobian matrix:')
jac_det = jac.det()
latexOut.addMath(sp.latex(jac_det))

latexOut.addSubsection('Strain-displacement matrix (B)')
print(dt.datetime.now().strftime('%X') + '\t' + 'Calculating B matrix.')
Btemp = jac_inv * sp.Matrix([dN_dxi,dN_deta])

B4 = sp.zeros(3, 8)
for i in range(0, 2):
    if i == 0:
        k = 0
        for j in range(0, 8):
            if(j%2 == 0):
                B4[i,j] = Btemp[i,k]
                k+=1
            else:
                B4[2, j] = Btemp[i, k-1]
    else:
        k = 0
        for j in range(0, 8):
            if(j%2 != 0):
                B4[i, j] = Btemp[i, k]
                k+=1
            else:
                B4[2, j] = Btemp[i, k]

latexOut.addMath(sp.latex(B4))

# THE TRIPLE PRODUCT (TP)
print(dt.datetime.now().strftime('%X') + '\t' + 'Calculating the triple product.')
TP4 = B4.T*D*B4

latexOut.addSubsection('Stiffness matrix (K)')
print(dt.datetime.now().strftime('%X') + '\t' + 'Calculating stiffness matrix.')
K4 = t*jac_det*(TP4.subs([(xi,-1),(eta,-1)])+TP4.subs([(xi,-1),(eta,1)])+TP4.subs([(xi,1),(eta,-1)])+\
               TP4.subs([(xi,1),(eta,1)]))
latexOut.addMath(sp.latex(K4))
    
latexOut.addSubsection('Nodal displacements')
print(dt.datetime.now().strftime('%X') + '\t' + 'Calculating nodal displacements.')
u1, v1, u2, v2, u3, v3, u4, v4 = sp.symbols("u_1 v_1 u_2 v_2 u_3 v_3 u_4 v_4")
u4_i = sp.Matrix([[u1],[v1],[u2],[v2],[u3],[v3],[u4],[v4]])
latexOut.addMath(sp.latex(u4_i))

latexOut.addSubsection('Strains')
print(dt.datetime.now().strftime('%X') + '\t' + 'Calculating strains.')
eps_x, eps_y, eps_xy = sp.symbols("\epsilon_x \epsilon_y \epsilon_{xy}")
eps4 = B4*u4_i
eps_x = eps4[0]
eps_y = eps4[1]
eps_xy = eps4[2]
latexOut.addMath(sp.latex(eps4))

latexOut.addSubsection('Stresses')
print(dt.datetime.now().strftime('%X') + '\t' + 'Calculating stresses.')
sig_x, sig_y, sig_xy = sp.symbols("\sigma_x \sigma_y \sigma_{xy}")
sig4 = D*eps4
sig_x = sig4[0]
sig_y = sig4[1]
sig_xy = sig4[2]
latexOut.addMath(sp.latex(eps4))

latexOut.finish()
print(dt.datetime.now().strftime('%X') + '\t' + 'Finished!')