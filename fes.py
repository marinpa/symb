# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 12:55:48 2021

@author: mpalaversa
"""

import datetime as dt
import latexout as lo
# List of all available FEs and their corresponding DOFs (this list should be updated after a new FE is added)
fes = {
        'PlnStrssQd1': [0, 1],
        'PlnStrssQd1Red1': [0, 1],
        'PlnStrssQd2': [0, 1],
        'PlnStrssQd2Red1': [0, 1],
        'PlnStrssQd2Red1Rot2': [0, 1, 2]
    }

def getFE(elementName):
    return fes[elementName]

class Fes:
    """
    Base class for all finite elements.
    """
    def __init__(self, saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis):
        self.latexOutput = False
        self.numericalAnalysis = bool(numericalAnalysis)
        
        self.printAnalysisDetails = bool(printAnalysisDetails)
        if(printAnalysisDetails):
            print('Analysis details:\n' + dt.datetime.now().strftime('%x %X') + '\t' + 'Starting the analysis.')
        
        self.analysisDetailsMode = str(analysisDetailsMode).upper()
        self.saveAnalysisDetails = bool(saveAnalysisDetails)
        if(saveAnalysisDetails):
            self.generateAnalysisReport(analysisDetailsMode)
            
        self.saveData = False
        self.useSavedData = bool(useSavedData)
        
        if(numericalAnalysis):
            self.ENum = 0
            self.nuNum = 0

        pass
        
    def generateAnalysisReport(self, analysisDetailsMode):
        """
        Prepares file for analysis report.

        """ 
        time = dt.datetime.now()
        self.analysisDetailsFileName = 'Analysis-Report-' + time.strftime('%Y') + '-' + time.strftime('%m') + '-' +\
                            time.strftime('%d') + '-' + time.strftime('%H') + '-' + time.strftime('%M') + '.txt'
        outputFile = open(self.analysisDetailsFileName, 'w')
        outputFile.write('Analysis started on ' + time.strftime('%Y') + '/' + time.strftime('%m') + '/' +\
                         time.strftime('%d') + ' at ' + time.strftime('%H') + ':' + time.strftime('%M') + '\n')
        if(self.analysisDetailsMode == 'U'):
            outputFile.write('Analysis report level: User\n')
        elif(self.analysisDetailsMode == 'D'):
            outputFile.write('Analysis report level: Debug\n')
        outputFile.close()
        
        return
    
    def setDataStoringOptions(self, saveData, maxTimeToSave):
        """
        Defines weather data from time consuming calculations is stored in separate files for later use.

        """
        self.saveData = saveData    
        if(saveData):
            self.maxTimeToSaveData = int(maxTimeToSave)
        
        return
    
    def generateLatexOutput(self, outputFileName, docType, paperSize, orientation, fontSize, language, title = '', author = ''):
        """
        Instantiates LatexOut object and sets latexFileName to user-defined.
        @param outputFileName, string - a user-defined name of the output Latex file (.tex)
        @param docType, string - type of Latex document (book, article...)
        @param paperSize, string - standard paper format (A4, A3...)
        @param orientation, string - document orientation (landscape, portrait)
        @param fontSize, string - just the value without unit, in points (pt)
        @param language, string
        @param title, string (optional) - if specified, a title page will be created
        @param author, string (only with title) - can be specified only if title is specified
        """
        self.latexOutput = True
        self.latexOut = lo.LatexOut(outputFileName, docType, paperSize, orientation, fontSize, language, title, author)
        self.latexFileName = outputFileName
        return
    
    def finishLatexOutput(self):
        self.latexOut.finish()
        return

    def isMatrixEmpty(self, matrix):
        for element in matrix:
            if(element != 0):
                return False
        return True
    
    def __del__(self):
        """
        A custom destructor for Fes class objects.

        """
        print(dt.datetime.now().strftime('%x %X') + '\t' + 'Analysis finished.')
        
        return