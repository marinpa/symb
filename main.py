import numpy as np
from elinterface import Element
from typing import List
from datetime import datetime
import sys, os
import time
import testmodels

# prepare model
# Straight cantilever beam

el_type='PlnStrssQd1Red1'
#el_type='PlnStrssQd1'
use_transformation = False
do_calc_stress = True
model_nodes,elnodeindices,eldata,bcdata,fdata,outputname,description = testmodels.straight_cantilever_mcneal_qd1(use_transformation)
#model_nodes,elnodeindices,eldata,bcdata,fdata,outputname,description = testmodels.straight_cantilever_mcneal_knjiga(use_transformation)
#model_nodes,elnodeindices,eldata,bcdata,fdata,outputname,description = testmodels.first(use_transformation)
#model_nodes,elnodeindices,eldata,bcdata,fdata,outputname,description = testmodels.straight_cantilever_mcneal_qd2rot(use_transformation)
outfilename = os.path.realpath('.') + '\\..\\output\\'+outputname+'_'+el_type+"-P.out"
#outfilename = 'output\\'+outputname+'_'+el_type+'.out'
#
dtstart = datetime.now()
print('FSB pyfemtest')
print('Output file:' + outfilename)
print('FSB test fem calculation started at: '+ str(dtstart.strftime("%d/%m/%Y %H:%M:%S")))
ts = time.perf_counter()


# prepare problem solution
unusednodes= set(range(len(model_nodes)))
i=0
calcelements = {}

for key, indices in elnodeindices.items():
    el_nodes = []
    for inod in indices:
        el_nodes.append(model_nodes[inod])
        unusednodes.discard(inod)
    calcel = Element(el_type,el_nodes,eldata,use_transformation)
    calcelements[key]=calcel
    ndofm = calcel.get_num_element_dofs()
if len(unusednodes)> 0:
    print("Error! Unused nodes exist. Program will ")
    print("List of unused nodes:",unusednodes)
    print("Program terminated due to Unused node error")
    exit()
# assumption, all nodes have the same dofs
dofs = calcel.get_node_dofs()
ndof = calcel.get_num_node_dofs()
nnods = len(model_nodes)
ndofm = nnods*ndof

alldofs = np.arange(ndofm)
K = np.zeros((ndofm,ndofm))
u = np.zeros((ndofm))
f = np.zeros((ndofm))
#for i in range(ndofm):
#    for j in range(ndofm):
#        K[i,j] =i*100+j
#dofs related to node i are from ndof*i to ndof*(i+1)-1
#Boundary condition
dofspec=[]
for bc in bcdata:
    i=bc[0]-1
    idof = bc[1]-1
    iddofmodel = ndof*i +idof
    dofspec.append(iddofmodel)
    u[iddofmodel]= bc[2]

doffree = np.delete(alldofs,dofspec)

#Nodal loads
for fc in fdata:
    i=fc[0]-1
    idof = fc[1]-1
    iddofmodel = ndof*i +idof
    f[iddofmodel]= fc[2]

# Assemble stiffness matrix
eldofs=[]
for iel in calcelements:
    calcel=calcelements[iel]
    indices = elnodeindices[iel]
    eldofs.clear()
    for i in indices:
        for j in range(ndof):
            eldofs.append(ndof*i+j)
    kel = calcel.get_element_stiff_matrix()
    K[np.ix_(eldofs, eldofs)] = K[np.ix_(eldofs, eldofs)] + kel

# Calculate displacements
#u[doffree] = np.linalg.solve(K[np.ix_(doffree,doffree)],fh[doffree])- np.dot(K[np.ix_(doffree,dofspec)],u[dofspec])
u[doffree] = np.linalg.solve(K[np.ix_(doffree,doffree)],f[doffree])
f[dofspec] = np.dot(K[[dofspec],:],u)

# Calculate stresses
element_results={}
if do_calc_stress:
    for iel in calcelements:
        calcel=calcelements[iel]
        indices = elnodeindices[iel]
        eldofs.clear()
        for i in indices:
            for j in range(ndof):
                eldofs.append(ndof * i + j)
        element_results[iel] = calcel.get_element_results(u[eldofs])

dt = time.perf_counter() - ts
print("Total calculation time, s:", dt)
print('FSB test fem calculation finished at: '+datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
with open(outfilename, "w") as fh:
    fh.write('--------------------------------------------------------------------------------' + '\n')
    fh.write('FSB pyfemtest' + '\n')
    fh.write('--------------------------------------------------------------------------------' + '\n')
    fh.write(description + '\n')
    fh.write('--------------------------------------------------------------------------------' + '\n')
    fh.write('Calculation started:' + str(dtstart.strftime("%d/%m/%Y %H:%M:%S")) + '\n')
    fh.write('Calculation ended:' + str(datetime.now().strftime("%d/%m/%Y %H:%M:%S")) + '\n')
    fh.write('Total calculation time, s:' + str(dt) + '\n')
    fh.write('--------------------------------------------------------------------------------' + '\n')
    fh.write('Nodes' + '\n')
    for i in range(len(model_nodes)):
        line = str(i+1)
        for xyz in model_nodes[i]:
            line += " " + str(xyz)
        fh.write(line + '\n')
    fh.write('Elements' + '\n')
    for id, el in elnodeindices.items():
        line = str(id)
        for item in el:
            line += " " + str(item+1)
        fh.write(line + '\n')
    fh.write('Element data' + '\n')
    line = el_type
    for i in range(len(eldata)):
        line += " " + str(eldata[i])
    fh.write(line + '\n')
    fh.write('Boundary conditions' + '\n')
    for i in range(len(bcdata)):
        line = ""
        for item in bcdata[i]:
            line += " " + str(item)
        fh.write(line + '\n')
    fh.write('Loads' + '\n')
    for i in range(len(fdata)):
        line = ""
        for item in fdata[i]:
            line += " " + str(item)
        fh.write(line + '\n')
    fh.write('--------------------------------------------------------------------------------' + '\n')
    fh.write('RESULTS' + '\n')
    fh.write('--------------------------------------------------------------------------------' + '\n')
    fh.write('Nodal displacements' + '\n')
    for i in range(len(model_nodes)):
        line = str(i+1)
        for idof in range(ndof):
            iddofmodel = ndof * i + idof
            line += " " + '{:.4e}'.format(u[iddofmodel])
        fh.write(line + '\n')
    fh.write('Nodal reactions' + '\n')
    dReactions = {}
    for bc in bcdata:
        i = bc[0]-1
        line = str(i+1)
        idof = bc[1]-1
        line += ' '+ str(idof+1)
        iddofmodel = ndof * i + idof
        line += ' ' + '{:.4e}'.format(f[iddofmodel])
        fh.write(line + '\n')
    if not do_calc_stress:
        sys.exit()
    fh.write('Element strains' + '\n')
    for elid in element_results:
        nlines = int(len(element_results[elid])/2)
        for j in range(nlines):
            line =str(elid)+ " " + str(element_results[elid][j])
            fh.write(line + '\n')
    fh.write('Element stresses' + '\n')
    for elid in element_results:
        nlines = int(len(element_results[elid])/2)
        for j in range(nlines, nlines*2):
            line =str(elid)+ " " + str(element_results[elid][j])
            fh.write(line + '\n')


