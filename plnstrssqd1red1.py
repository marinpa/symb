# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 17:17:43 2021

@author: mpalaversa
"""
import datetime as dt
import pickle as pick
import sympy as sp
import time as tm

from sys import getsizeof

import plnstrssqd1 as psq1

# Plane stress quadrialteral finite element with reduced integration of shear terms in strain-displacement matrix
class PlnStrssQd1Red1(psq1.PlnStrssQd1):
    """
    4-node plane stress quadrialteral finite element with reduced integration of shear terms in stiffness matrix.
    """
    def __init__(self, saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis = False):
        self.name = 'PlnStrssQd1Red1'
        super().__init__(saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis)
        self.reducedIntegrationShear = True
        pass
        
    def recalculateBMatrix(self):
        self.calculateBMatrix()
        self.BShearStrains = self.BMatrix.row(2)
        self.BMatrix.row_del(2)
        
        return
        
    def calculateStiffnessMatrix(self):
        if(self.isMatrixEmpty(self.BMatrix)):
            self.recalculateBMatrix()
            self.getDMatrix()
        elif(self.isMatrixEmpty(self.jacobianInverse)):
            self.calculateJacobianItsInverseAndDeterminant()
            
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating the triple product.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating the triple product.')
        
        if(self.saveData):
            stopwatchStart = tm.time()

        TPShearStrains = self.BShearStrains.T*self.plnStrssD[2,2]*self.BShearStrains
        plnStrssDTemp = sp.zeros(3, 3)
        for i in range(0, 9):
            plnStrssDTemp[i] = self.plnStrssD[i]

        plnStrssDTemp.row_del(2)
        plnStrssDTemp.col_del(2)
        TPNormalStrains = self.BMatrix.T*plnStrssDTemp*self.BMatrix
        
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'The triple product is calculated.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'The triple product is calculated.')

        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating stiffness matrix (K).\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating stiffness matrix (K).')
            
        stiffnessMatrixNormalStrains = self.t*(self.jacobianDeterminant.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])*TPNormalStrains.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])+\
                                                                self.jacobianDeterminant.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])*TPNormalStrains.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])+\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])*TPNormalStrains.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])+\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])])*TPNormalStrains.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])]))
        stiffnessMatrixShearStrains = 4*self.t*(self.jacobianDeterminant.subs([(self.xi,0),(self.eta,0)])*TPShearStrains.subs([(self.xi, 0),(self.eta, 0)]))
        self.stiffnessMatrix = stiffnessMatrixNormalStrains + stiffnessMatrixShearStrains
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) calculated.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) calculated.')
            
        if(self.saveData):
            stopwatchEnd = tm.time()
            if(int(stopwatchEnd - stopwatchStart) > self.maxTimeToSaveData):
                if(self.saveAnalysisDetails):
                    with open(self.analysisDetailsFileName, 'a') as outputFile:
                        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix  (K) saved because time for their calculation was ' +\
                                         str(int(stopwatchEnd-stopwatchStart)) + ' s, which is longer than set by the user (' + str(self.maxTimeToSaveData) + ' s).\n')
                if(self.printAnalysisDetails):
                    print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) saved to an external file.')
                with open(self.name + '-StiffMat.dat', 'wb') as outputFile:
                    outputFile.write(pick.dumps(self.stiffnessMatrix))
            
        if(self.latexOutput):
            self.latexOut.addSubsection('Stiffness matrix (K)')
            tempStr = sp.latex(self.stiffnessMatrix)
            if(getsizeof(tempStr) < 100000):
                self.latexOut.addMath(r'\left[k^e\right] = ' + tempStr)
                tempStr = ''
            else:
                self.latexOut.addText('Latex output was not generated for the stiffness matrix (K) beacuse its size was greater than 100 kB.')
                if(self.saveAnalysisDetails):
                    with open(self.analysisDetailsFileName, 'a') as outputFile:
                        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the stiffness matrix (K) because its size was greater than 100 kB.\n')
                if(self.printAnalysisDetails):
                    print(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the stiffness matrix (K) because its size was greater than 100 kB.')
        
        return
