# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 12:55:48 2021

@author: mpalaversa
"""
import datetime as dt
import numpy as np
import pickle as pick
import sympy as sp
import time as tm

import fes

class PlnStrssElement(fes.Fes):
    """
    Base class for plane stress finite elements.
    """
    def __init__(self, saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis):
        super().__init__(saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis)
        
        self.jacobianInverse = sp.zeros(2, 2)
        self.jacobianInverse = sp.zeros(2, 2)
        self.jacobianDeterminant = ''
        
        if(useSavedData):
            self.readDataFromFile()
        
        self.strains = sp.zeros(3, 1)
        self.stresses = sp.zeros(3, 1)
        
        self.eps_x, self.eps_y, self.eps_xy = sp.symbols('\epsilon_x \epsilon_y \epsilon_{xy}')
        self.sig_x, self.sig_y, self.sig_xy = sp.symbols('\sigma_x \sigma_y \sigma_{xy}')
        
        self.plnStrssD = sp.zeros(3, 3)
        
        if(numericalAnalysis):
            self.tNum = 0
            self.strainsNum = np.empty([3, 1])
            self.stressesNum = np.empty([3, 1])
        
        self.reducedIntegrationShear = False
        pass
        
    def readDataFromFile(self):
        try:
            with open(self.name + '-JacInv.dat', 'rb') as inputFile:
                self.jacobianInverse = pick.loads(inputFile.read())
                
            if(self.saveAnalysisDetails):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian matrix inverse imported from a previous analysis.\n')
            if(self.printAnalysisDetails):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian matrix inverse imported from a previous analysis.')
        except OSError:
            if(self.saveAnalysisDetails):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian matrix inverse is not found in data stored in a previous analysis.\n')
            if(self.printAnalysisDetails):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian matrix inverse is not found in data stored in a previous analysis.')
        except:
            if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'An unexpected error when looking for Jacobian matrix inverse file.\n')
            if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'An unexpected error when looking for Jacobian matrix inverse file.')
            
        try:
            with open(self.name + '-JacDet.dat', 'rb') as inputFile:
                self.jacobianDeterminant = pick.loads(inputFile.read())
                
            if(self.saveAnalysisDetails):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian matrix determinant imported from a previous analysis.\n')
                if(self.printAnalysisDetails):
                    print(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian matrix determinant imported from a previous analysis.')
        except OSError:
            if(self.saveAnalysisDetails):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian matrix determinant is not found in data stored in a previous analysis.\n')
                if(self.printAnalysisDetails):
                    print(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian matrix determinant is not found in data stored in a previous analysis.')
        except:
            if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'An unexpected error when looking for Jacobian matrix determinant file.\n')
            if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
                print('An unexpected error has occured while looking for Jacobian matrix determinant file.')
        
        try:
            with open(self.name + '-BMat.dat', 'rb') as inputFile:
                self.BMatrix = pick.loads(inputFile.read())
                if(self.latexOutput):
                    self.latexOut.addMath(r'\left[B\right] = ' + sp.latex(self.BMatrix))
                
            if(self.saveAnalysisDetails):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Strain-displacement matrix (B) imported from a previous analysis.\n')
            if(self.printAnalysisDetails):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'Strain-displacement matrix (B) imported from a previous analysis.')
        except OSError:
            if(self.saveAnalysisDetails):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Strain-displacement matrix (B) is not found in data stored in a previous analysis.\n')
            if(self.printAnalysisDetails):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'Strain-displacement matrix (B) is not found in data stored in a previous analysis.')
        except:
            if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'An unexpected error when looking for strain-displacement matrix (B) file.\n')
            if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'An unexpected error while looking for strain-displacement matrix (B) file.')
            
        try:
            with open(self.name + '-StiffMat.dat', 'rb') as inputFile:
                self.stiffnessMatrix = pick.loads(inputFile.read())
                
            if(self.saveAnalysisDetails):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) imported from a previous analysis.\n')
            if(self.printAnalysisDetails):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) imported from a previous analysis.')
        except OSError:
            if(self.saveAnalysisDetails):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) is not found in data stored in a previous analysis.\n')
            if(self.printAnalysisDetails):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) is not found in data stored in a previous analysis.')
        except:
            if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'An unexpected error when looking for stiffness matrix (K) file.\n')
            if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'An unexpected error while looking for stiffness matrix (K) file.')
        return
    
    def setCommonVariables(self):
        self.Ee, self.nu = sp.symbols(r"E,\nu")
    
        if(self.latexOutput):
            self.latexOut.addSection('Common variables')
            self.latexOut.addText('Material characteristics Young\'s modulus and Poissons coefficient:')
            self.latexOut.addMath(sp.latex(self.Ee))
            self.latexOut.addMath(sp.latex(self.nu))
        
        self.x, self.y, self.xi, self.eta = sp.symbols(r"x,y,\xi,\eta")
    
        if(self.latexOutput):
            self.latexOut.addText('Metric and parametric coordinates:')
            self.latexOut.addMath(sp.latex(self.x))
            self.latexOut.addMath(sp.latex(self.y))
            self.latexOut.addMath(sp.latex(self.xi))
            self.latexOut.addMath(sp.latex(self.eta))
        
        self.t = sp.symbols("t")
    
        if(self.latexOutput):
            self.latexOut.addText('Thickness:')
            self.latexOut.addMath(sp.latex(self.t))
        
        # Coordinates of Gauss points and weight factors (in case of 4 GPs all weight factors are 1)
        if(self.numberOfGPs == 4):
            self.gp[0, 0] = -sp.sqrt(3)/3
            self.gp[0, 1] = -sp.sqrt(3)/3
            self.gp[1, 0] = -sp.sqrt(3)/3
            self.gp[1, 1] = sp.sqrt(3)/3
            self.gp[2, 0] = sp.sqrt(3)/3
            self.gp[2, 1] = -sp.sqrt(3)/3
            self.gp[3, 0] = sp.sqrt(3)/3
            self.gp[3, 1] = sp.sqrt(3)/3
        elif(self.numberOfGPs == 9):
            self.gp[0, 0] = -sp.sqrt(3/5)
            self.gp[0, 1] = -sp.sqrt(3/5)
            self.wf[0, 0] = 25/81
            self.gp[1, 0] = -sp.sqrt(3/5)
            self.gp[1, 1] = 0
            self.wf[1, 0] = 40/81
            self.gp[2, 0] = -sp.sqrt(3/5)
            self.gp[2, 1] = sp.sqrt(3/5)
            self.wf[2, 0] = 25/81
            self.gp[3, 0] = 0
            self.gp[3, 1] = sp.sqrt(3/5)
            self.wf[3, 0] = 40/81
            self.gp[4, 0] = sp.sqrt(3/5)
            self.gp[4, 1] = sp.sqrt(3/5)
            self.wf[4, 0] = 25/81
            self.gp[5, 0] = sp.sqrt(3/5)
            self.gp[5, 1] = 0
            self.wf[5, 0] = 40/81
            self.gp[6, 0] = sp.sqrt(3/5)
            self.gp[6, 1] = -sp.sqrt(3/5)
            self.wf[6, 0] = 25/81
            self.gp[7, 0] = 0
            self.gp[7, 1] = -sp.sqrt(3/5)
            self.wf[7, 0] = 40/81
            self.gp[8, 0] = 0
            self.gp[8, 1] = 0
            self.wf[8, 0] = 64/81
        
        if(self.latexOutput):
            self.latexOut.addSection('Derivation of element\'s stifness matrix')
        
        return
    
    def getDMatrix(self):
        self.plnStrssD = (self.Ee/(1-self.nu**2))*sp.Matrix([[1,self.nu,0],[self.nu,1,0],[0,0,(1-self.nu)/2]])
        if(self.latexOutput):
            self.latexOut.addSubsection('Constitutive matrix (based on plane stress assumption of 2D elasticity):')
            self.latexOut.addMath(r'\left[D\right] = ' + sp.latex(self.plnStrssD))
        
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Constitutive matrix (D) defined.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Constitutive matrix (D) defined.')
        
        return
    
    def calculateDerivativesOfShapeFunctionByParametricCoordinates(self):
        if(self.isMatrixEmpty(self.shapeFunction)):
            self.calculateShapeFunction()
        
        self.dN_dxi = sp.diff(self.shapeFunction, self.xi)
        self.dN_deta = sp.diff(self.shapeFunction, self.eta)
        
        if(self.latexOutput):
            self.latexOut.addText('Derivatives of shape functions by parametric coordinates:')
            self.latexOut.addMath(r'\frac{d\left[N\right]}{d\xi} = ' + sp.latex(self.dN_dxi))
            self.latexOut.addMath(r'\frac{d\left[N\right]}{d\eta} = ' + sp.latex(self.dN_deta))
            
        return
    
    def calculateJacobianItsInverseAndDeterminant(self):
        self.calculateDerivativesOfShapeFunctionByParametricCoordinates()
            
        if(self.latexOutput):
            self.latexOut.addSubsection('Jacobian matrix, its inverse and determinant')
        
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating Jacobian matrix, its inverse and determinant.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating Jacobian, its inverse and determinant.')

        if(self.saveData):
            stopwatchStart = tm.time()
        
        # Creates a temporary matrix where nodal coordinates related to x- and y-directions respectively are stored.
        xx_i = sp.zeros(int(self.x_i.shape[0]/2), self.x_i.shape[1])
        xy_i = sp.zeros(int(self.x_i.shape[0]/2), self.x_i.shape[1])
        
        k = 0
        l = 0
        # Copies elements of total vector of nodal coordinates to the above defined vector of coordinates in two directions.
        for i in range(0, self.x_i.shape[0]):
            if(i%2 == 0):
                xx_i[k, 0] = self.x_i[i, 0]
                k += 1
            else:
                xy_i[l, 0] = self.x_i[i, 0]
                l += 1
        
        # Derivatives of coordinates in metric space by coordinates in parametric space.
        dx_dxi = self.dN_dxi * xx_i
        dy_dxi = self.dN_dxi * xy_i
        dx_deta = self.dN_deta * xx_i
        dy_deta = self.dN_deta * xy_i
        
        if(self.latexOutput):
            self.latexOut.addText('Derivatives of metric by parametric coordinates:')
            self.latexOut.addMath(r'\frac{dx}{d\xi} = ' + sp.latex(dx_dxi))
            self.latexOut.addMath(r'\frac{dy}{d\xi} = ' + sp.latex(dy_dxi))
            self.latexOut.addMath(r'\frac{dx}{d\eta} = ' + sp.latex(dx_deta))
            self.latexOut.addMath(r'\frac{dy}{d\eta} = ' + sp.latex(dy_deta))
            
        # Defines the Jacobian matrix.
        jac = sp.Matrix([[dx_dxi, dy_dxi],[dx_deta, dy_deta]])
        
        if(self.latexOutput):
            self.latexOut.addText('Jacobian matrix:')
            self.latexOut.addMath(r'\left[J\right] = ' + sp.latex(jac))
            
        # Calculates inverse of the Jacobian matrix.
        self.jacobianInverse = jac.inv('ADJ')
        
        if(self.latexOutput):
            self.latexOut.addText('Inverse of the Jacobian matrix:')
            self.latexOut.addMath(r'\left[J\right]^{-1} = ' + sp.latex(self.jacobianInverse))
        
        # Calculates determinant of the Jacobian matrix.
        self.jacobianDeterminant = jac.det()
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian matrix, its inverse and determinant calculated.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Jacobian, its inverse and determinant calculated.')
        
        if(self.saveData):
            stopwatchEnd = tm.time()
            if(int(stopwatchEnd-stopwatchStart) > self.maxTimeToSaveData):
                if(self.saveAnalysisDetails):
                    with open(self.analysisDetailsFileName, 'a') as outputFile:
                        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Inverse and determinant of the Jacobian matrix saved because time for their calculation was ' +\
                                         str(int(stopwatchEnd-stopwatchStart)) + ' s, which is longer than set by the user (' + str(self.maxTimeToSaveData) + ' s).\n')
                if(self.printAnalysisDetails):
                    print(dt.datetime.now().strftime('%x %X') + '\t' + 'Inverse and determinant of the Jacobian matrix saved to an external file.')
                with open(self.name + '-JacInv.dat', 'wb') as outputFile:
                    outputFile.write(pick.dumps(self.jacobianInverse))
                with open(self.name + '-JacDet.dat', 'wb') as outputFile:
                    outputFile.write(pick.dumps(self.jacobianDeterminant))
        
        if(self.latexOutput):
            self.latexOut.addText('Determinant of the Jacobian matrix:')
            self.latexOut.addMath(r'det\left[J\right] = ' + sp.latex(self.jacobianDeterminant))
        
        return
    
    def calculateBMatrix(self):
        if(self.isMatrixEmpty(self.jacobianInverse)):
            self.calculateJacobianItsInverseAndDeterminant()
        else:
            self.calculateDerivativesOfShapeFunctionByParametricCoordinates()
        
        if(self.latexOutput):
            self.latexOut.addSubsection('Strain-displacement matrix (B)')
        
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating the strain-displacement matrix (B).\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating B matrix.')
        
        if(self.saveData):
            stopwatchStart = tm.time()
        # Btemp is a 2x4 matrix that contains shape function's derivatives by \xi in the first row and by \eta
        # in the second row.
        Btemp = self.jacobianInverse * sp.Matrix([self.dN_dxi,self.dN_deta])
        for i in range(0, self.BMatrix.shape[0] - 1):
            if i == 0:
                k = 0
                for j in range(0, self.BMatrix.shape[1]):
                    if(j%2 == 0):
                        self.BMatrix[i,j] = Btemp[i,k]
                        k+=1
                    else:
                        self.BMatrix[2, j] = Btemp[i, k-1]
            else:
                k = 0
                for j in range(0, self.BMatrix.shape[1]):
                    if(j%2 != 0):
                        self.BMatrix[i, j] = Btemp[i, k]
                        k+=1
                    else:
                        self.BMatrix[2, j] = Btemp[i, k]
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Strain-displacement matrix (B) calculated.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'B matrix calculated.')
        
        if(self.saveData):
            stopwatchEnd = tm.time()
            if(int(stopwatchEnd - stopwatchStart) > self.maxTimeToSaveData):
                with open(self.name + '-BMat.dat', 'wb') as outputFile:
                    outputFile.write(pick.dumps(self.BMatrix))
        
        if(self.latexOutput):
            tempStr = sp.latex(self.BMatrix)
            if(len(tempStr) < 100000):
                self.latexOut.addMath(r'\left[B\right] = ' + tempStr)
                tempStr = ''
            else:
                self.latexOut.addText('Latex output was not generated for the strain-displacement matrix (B) beacuse its size was greater than 100 kB.')
                if(self.saveAnalysisDetails):
                    with open(self.analysisDetailsFileName, 'a') as outputFile:
                        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the strain-displacement matrix (B) beacuse its size was greater than 100 kB.\n')
                if(self.printAnalysisDetails):
                    print(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the strain-displacement matrix (B) beacuse its size was greater than 100 kB.')
            
        return
    
    def calculateStiffnessMatrix(self):
        if(self.isMatrixEmpty(self.BMatrix)):
            if(self.name == 'PlnStrssQd2Rot2'):
                self.recalculateBMatrix()
            else:
                self.calculateBMatrix()
            
            self.getDMatrix()
        elif(self.isMatrixEmpty(self.jacobianInverse)):
            self.calculateJacobianItsInverseAndDeterminant()
            
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating the triple product.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating the triple product.')

        if(self.saveData):
            stopwatchStart = tm.time()

        TP = self.BMatrix.T*self.plnStrssD*self.BMatrix

        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'The triple product is calculated.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'The triple product is calculated.')

        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating stiffness matrix (K).\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating stiffness matrix (K).')
            
        if(self.numberOfGPs == 4):
            self.stiffnessMatrix = self.t*(self.jacobianDeterminant.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])*TP.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])+\
                                                                self.jacobianDeterminant.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])*TP.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])+\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])*TP.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])+\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])])*TP.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])]))
        elif(self.numberOfGPs == 9):
            self.stiffnessMatrix = self.t*(self.jacobianDeterminant.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])*TP.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])*self.wf[0, 0] +\
                                                                self.jacobianDeterminant.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])*TP.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])*self.wf[1, 0] +\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])*TP.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])*self.wf[2, 0] +\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])])*TP.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])])*self.wf[3, 0] +\
                                                                        self.jacobianDeterminant.subs([(self.xi,self.gp[4, 0]),(self.eta,self.gp[4, 1])])*TP.subs([(self.xi,self.gp[4, 0]),(self.eta,self.gp[4, 1])])*self.wf[4, 0] +\
                                                                            self.jacobianDeterminant.subs([(self.xi,self.gp[5, 0]),(self.eta,self.gp[5, 1])])*TP.subs([(self.xi,self.gp[5, 0]),(self.eta,self.gp[5, 1])])*self.wf[5, 0] +\
                                                                                self.jacobianDeterminant.subs([(self.xi,self.gp[6, 0]),(self.eta,self.gp[6, 1])])*TP.subs([(self.xi,self.gp[6, 0]),(self.eta,self.gp[6, 1])])*self.wf[6, 0] +\
                                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[7, 0]),(self.eta,self.gp[7, 1])])*TP.subs([(self.xi,self.gp[7, 0]),(self.eta,self.gp[7, 1])])*self.wf[7, 0] +\
                                                                                        self.jacobianDeterminant.subs([(self.xi,self.gp[8, 0]),(self.eta,self.gp[8, 1])])*TP.subs([(self.xi,self.gp[8, 0]),(self.eta,self.gp[8, 1])])*self.wf[8, 0])

        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) calculated.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) calculated.')
            
        if(self.saveData):
            stopwatchEnd = tm.time()
            if(int(stopwatchEnd - stopwatchStart) > self.maxTimeToSaveData):
                if(self.saveAnalysisDetails):
                    with open(self.analysisDetailsFileName, 'a') as outputFile:
                        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix  (K) saved because time for their calculation was ' +\
                                         str(int(stopwatchEnd-stopwatchStart)) + ' s, which is longer than set by the user (' + str(self.maxTimeToSaveData) + ' s).\n')
                if(self.printAnalysisDetails):
                    print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix (K) saved to an external file.')
                with open(self.name + '-StiffMat.dat', 'wb') as outputFile:
                    outputFile.write(pick.dumps(self.stiffnessMatrix))
            
        if(self.latexOutput):
            self.latexOut.addSubsection('Stiffness matrix (K)')
            tempStr = sp.latex(self.stiffnessMatrix)
            if(len(tempStr) < 100000):
                self.latexOut.addMath(r'\left[k^e\right] = ' + tempStr)
                tempStr = ''
            else:
                self.latexOut.addText('Latex output was not generated for the stiffness matrix (K) beacuse its size was greater than 100 kB.')
                if(self.saveAnalysisDetails):
                    with open(self.analysisDetailsFileName, 'a') as outputFile:
                        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the stiffness matrix (K) because its size was greater than 100 kB.\n')
                if(self.printAnalysisDetails):
                    print(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the stiffness matrix (K) because its size was greater than 100 kB.')
        
        return
    
    def calculateStrains(self):
        if(self.isMatrixEmpty(self.BMatrix)):
            self.calculateBMatrix()
        
        if(self.name == 'PlnStrssQd2Rot2'  and self.nodalDisplacements.shape[0] != 12):
            self.reinitialiseElement()
        
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating strains.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating strains.')
            
        if(self.reducedIntegrationShear):
            self.BMatrix = self.BMatrix.row_insert(2, self.BShearStrains)
        
        self.strains = self.BMatrix * self.nodalDisplacements
        
        if(self.latexOutput):
            self.latexOut.addSection('Strains')
            # This is commented because when all strain components are printed together they cannot fit the page.
            # Uncomment this when that problem is solved.
            #tempStr = sp.latex(self.strains)
            #if(len(tempStr) < 100000):
             #   eps = sp.Matrix([[self.eps_x],[self.eps_y],[self.eps_xy]])
              #  self.latexOut.addMath(sp.latex(eps) + ' = '  + tempStr)
               # tempStr = ''
            #else:
             #   self.latexOut.addText('Latex output was not generated for the strains because its size was greater than 100 kB.')
              #  if(self.saveAnalysisDetails):
               #     with open(self.analysisDetailsFileName, 'a') as outputFile:
                #        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the strains because its size was greater than 100 kB.\n')
                #if(self.printAnalysisDetails):
                 #   print(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the strains because its size was greater than 100 kB.')
            # Comment out the following directives for latex output once the previous block is uncommented.
            self.latexOut.addMath(sp.latex(sp.Matrix([self.eps_x])) + ' = '  + sp.latex(self.strains[0, 0]))
            self.latexOut.addMath(sp.latex(sp.Matrix([self.eps_y])) + ' = '  + sp.latex(self.strains[1, 0]))
            self.latexOut.addMath(sp.latex(sp.Matrix([self.eps_xy])) + ' = '  + sp.latex(self.strains[2, 0]))
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Strains are calculated.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Strains are calculated.')
            
        return
    
    def calculateStresses(self):
        if(self.isMatrixEmpty(self.BMatrix)):
            self.calculateBMatrix()
        
        if(self.name == 'PlnStrssQd2Rot2' and self.nodalDisplacements.shape[0] != 12):
            self.reinitialiseElement()
        
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating stresses.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating stresses.')
            
        self.stresses = self.plnStrssD * self.BMatrix * self.nodalDisplacements
            
        if(self.latexOutput):
            self.latexOut.addSection('Stresses')
            # This is commented because when all stress components are printed together they cannot fit the page.
            # Uncomment this when that problem is solved.
            #tempStr = sp.latex(self.stresses)
            #if(len(tempStr) < 100000):
             #   sig = sp.Matrix([[self.sig_x],[self.sig_y],[self.sig_xy]])
              #  self.latexOut.addMath(sp.latex(sig) + ' = '  + tempStr)
               # tempStr = ''
            #else:
             #   self.latexOut.addText('Latex output was not generated for the stresses beacuse its size was greater than 100 kB.')
              #  if(self.saveAnalysisDetails):
               #     with open(self.analysisDetailsFileName, 'a') as outputFile:
                #        outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the stresses because its size was greater than 100 kB.\n')
                #if(self.printAnalysisDetails):
                 #   print(dt.datetime.now().strftime('%x %X') + '\t' + 'Latex output was not generated for the stresses because its size was greater than 100 kB.')
        
            # Comment out the following directives for latex output once the previous block is uncommented.
            self.latexOut.addMath(sp.latex(sp.Matrix([self.sig_x])) + ' = '  + sp.latex(self.stresses[0, 0]))
            self.latexOut.addMath(sp.latex(sp.Matrix([self.sig_y])) + ' = '  + sp.latex(self.stresses[1, 0]))
            self.latexOut.addMath(sp.latex(sp.Matrix([self.sig_xy])) + ' = '  + sp.latex(self.stresses[2, 0]))
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stresses are calculated.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stresses are calculated.')
        
        return
    
    def get_strains_and_stresses(self, nodesCoordinatesList, nodalDisplacements, eldata, outputAt):
        """
        Evaluates expressions for strains and stresses on numerical values of coordinates of element vertices,
        displacements, Young's modulus and Poisson's coefficient (all of these are supplied by an external program).

        Parameters
        ----------
        nodesCoordinatesList : list
            Coordinates of element's vertices (numerical values) from an external program.
        nodalDisplacements : numpy ndarray
            Nodal displacements (numerical values) from structural analysis in an external program.
        eldata : list
            Young's modulus and Poisson's coefficient (supplied by an external program).
        outputAt : list
            Position of strains' and stresses' recovery points ('GP' for Gauss points, 'N' for nodes, 'C' for centre).

        Returns
        -------
        None.

        """
        if(self.isMatrixEmpty(self.strains)):
            self.calculateStrains()
        
        if(self.isMatrixEmpty(self.stresses)):
            self.calculateStresses()
        
        EeNum = eldata[0]
        nuNum = eldata[1]
        
        if(self.nodalDisplacements.shape[0] == 8 or self.nodalDisplacements.shape[0] == 12):
            # Creates numpy array where numerical values of element vertices' coordinates will be stored.
            nodesCoordinatesNum = np.empty([4, 2])
            nodesCoordinatesNum[0] = nodesCoordinatesList[0]
            nodesCoordinatesNum[1] = nodesCoordinatesList[1]
            nodesCoordinatesNum[2] = nodesCoordinatesList[2]
            nodesCoordinatesNum[3] = nodesCoordinatesList[3]
        elif(self.nodalDisplacements.shape[0] == 16):
            nodesCoordinatesNum = np.empty([8, 2])
            nodesCoordinatesNum[0] = nodesCoordinatesList[0]
            nodesCoordinatesNum[1] = nodesCoordinatesList[1]
            nodesCoordinatesNum[2] = nodesCoordinatesList[2]
            nodesCoordinatesNum[3] = nodesCoordinatesList[3]
            nodesCoordinatesNum[4] = nodesCoordinatesList[4]
            nodesCoordinatesNum[5] = nodesCoordinatesList[5]
            nodesCoordinatesNum[6] = nodesCoordinatesList[6]
            nodesCoordinatesNum[7] = nodesCoordinatesList[7]
        
        strains = []
        stresses = []
        
        #if(self.reducedIntegrationShear):
         #       shearStrains = self.strains.row(2)
          #      shearStresses = self.stresses.row(2)
        
        if(self.nodalDisplacements.shape[0] == 8):
                tempExprStrains = self.strains.subs([(self.x1, nodesCoordinatesNum[0, 0]), (self.y1, nodesCoordinatesNum[0, 1]),\
                                                     (self.nodalDisplacements[0], nodalDisplacements[0]), (self.nodalDisplacements[1], nodalDisplacements[1]),\
                                                         (self.x2, nodesCoordinatesNum[1, 0]), (self.y2, nodesCoordinatesNum[1, 1]),\
                                                             (self.nodalDisplacements[2], nodalDisplacements[2]), (self.nodalDisplacements[3], nodalDisplacements[3]),\
                                                                 (self.x3, nodesCoordinatesNum[2, 0]), (self.y3, nodesCoordinatesNum[2, 1]),\
                                                                     (self.nodalDisplacements[4], nodalDisplacements[4]), (self.nodalDisplacements[5], nodalDisplacements[5]),\
                                                                         (self.x4, nodesCoordinatesNum[3, 0]), (self.y4, nodesCoordinatesNum[3, 1]),\
                                                                             (self.nodalDisplacements[6], nodalDisplacements[6]), (self.nodalDisplacements[7], nodalDisplacements[7])])
                tempExprStrainsNum = sp.lambdify([self.xi, self.eta], tempExprStrains, 'numpy')
            
                tempExprStresses = self.stresses.subs([(self.Ee, EeNum), (self.nu, nuNum),\
                                                       (self.x1, nodesCoordinatesNum[0, 0]), (self.y1, nodesCoordinatesNum[0, 1]),\
                                                           (self.nodalDisplacements[0], nodalDisplacements[0]), (self.nodalDisplacements[1], nodalDisplacements[1]),\
                                                               (self.x2, nodesCoordinatesNum[1, 0]), (self.y2, nodesCoordinatesNum[1, 1]),\
                                                                   (self.nodalDisplacements[2], nodalDisplacements[2]), (self.nodalDisplacements[3], nodalDisplacements[3]),\
                                                                       (self.x3, nodesCoordinatesNum[2, 0]), (self.y3, nodesCoordinatesNum[2, 1]),\
                                                                           (self.nodalDisplacements[4], nodalDisplacements[4]), (self.nodalDisplacements[5], nodalDisplacements[5]),\
                                                                               (self.x4, nodesCoordinatesNum[3, 0]), (self.y4, nodesCoordinatesNum[3, 1]),\
                                                                                   (self.nodalDisplacements[6], nodalDisplacements[6]), (self.nodalDisplacements[7], nodalDisplacements[7])])
                tempExprStressesNum = sp.lambdify([self.xi, self.eta], tempExprStresses, 'numpy')
        elif(self.nodalDisplacements.shape[0] == 12):
                tempExprStrains = self.strains.subs([(self.x1, nodesCoordinatesNum[0, 0]), (self.y1, nodesCoordinatesNum[0, 1]),\
                                             (self.nodalDisplacements[0], nodalDisplacements[0]), (self.nodalDisplacements[1], nodalDisplacements[1]),\
                                                 (self.nodalDisplacements[2], nodalDisplacements[2]),\
                                             (self.x2, nodesCoordinatesNum[1, 0]), (self.y2, nodesCoordinatesNum[1, 1]),\
                                             (self.nodalDisplacements[3], nodalDisplacements[3]), (self.nodalDisplacements[4], nodalDisplacements[4]),\
                                                 (self.nodalDisplacements[5], nodalDisplacements[5]),\
                                             (self.x3, nodesCoordinatesNum[2, 0]), (self.y3, nodesCoordinatesNum[2, 1]),\
                                             (self.nodalDisplacements[6], nodalDisplacements[6]), (self.nodalDisplacements[7], nodalDisplacements[7]),\
                                                 (self.nodalDisplacements[8], nodalDisplacements[8]),\
                                             (self.x4, nodesCoordinatesNum[3, 0]), (self.y4, nodesCoordinatesNum[3, 1]),\
                                             (self.nodalDisplacements[9], nodalDisplacements[9]), (self.nodalDisplacements[10], nodalDisplacements[10]),\
                                                 (self.nodalDisplacements[11], nodalDisplacements[11])])
                tempExprStrainsNum = sp.lambdify([self.xi, self.eta], tempExprStrains, 'numpy')
            
                tempExprStresses = self.stresses.subs([(self.Ee, EeNum), (self.nu, nuNum),\
                                                       (self.x1, nodesCoordinatesNum[0, 0]), (self.y1, nodesCoordinatesNum[0, 1]),\
                                             (self.nodalDisplacements[0], nodalDisplacements[0]), (self.nodalDisplacements[1], nodalDisplacements[1]),\
                                                 (self.nodalDisplacements[2], nodalDisplacements[2]),\
                                             (self.x2, nodesCoordinatesNum[1, 0]), (self.y2, nodesCoordinatesNum[1, 1]),\
                                             (self.nodalDisplacements[3], nodalDisplacements[3]), (self.nodalDisplacements[4], nodalDisplacements[4]),\
                                                 (self.nodalDisplacements[5], nodalDisplacements[5]),\
                                             (self.x3, nodesCoordinatesNum[2, 0]), (self.y3, nodesCoordinatesNum[2, 1]),\
                                             (self.nodalDisplacements[6], nodalDisplacements[6]), (self.nodalDisplacements[7], nodalDisplacements[7]),\
                                                 (self.nodalDisplacements[8], nodalDisplacements[8]),\
                                             (self.x4, nodesCoordinatesNum[3, 0]), (self.y4, nodesCoordinatesNum[3, 1]),\
                                             (self.nodalDisplacements[9], nodalDisplacements[9]), (self.nodalDisplacements[10], nodalDisplacements[10]),\
                                                 (self.nodalDisplacements[11], nodalDisplacements[11])])
                tempExprStressesNum = sp.lambdify([self.xi, self.eta], tempExprStresses, 'numpy')
        elif(self.nodalDisplacements.shape[0] == 16):
                tempExprStrains = self.strains.subs([(self.x1, nodesCoordinatesNum[0, 0]), (self.y1, nodesCoordinatesNum[0, 1]),\
                                             (self.nodalDisplacements[0], nodalDisplacements[0]), (self.nodalDisplacements[1], nodalDisplacements[1]),\
                                             (self.x2, nodesCoordinatesNum[1, 0]), (self.y2, nodesCoordinatesNum[1, 1]),\
                                             (self.nodalDisplacements[2], nodalDisplacements[2]), (self.nodalDisplacements[3], nodalDisplacements[3]),\
                                             (self.x3, nodesCoordinatesNum[2, 0]), (self.y3, nodesCoordinatesNum[2, 1]),\
                                             (self.nodalDisplacements[4], nodalDisplacements[4]), (self.nodalDisplacements[5], nodalDisplacements[5]),\
                                             (self.x4, nodesCoordinatesNum[3, 0]), (self.y4, nodesCoordinatesNum[3, 1]),\
                                             (self.nodalDisplacements[6], nodalDisplacements[6]), (self.nodalDisplacements[7], nodalDisplacements[7]),\
                                             (self.x5, nodesCoordinatesNum[4, 0]), (self.y5, nodesCoordinatesNum[4, 1]),\
                                             (self.nodalDisplacements[8], nodalDisplacements[8]), (self.nodalDisplacements[9], nodalDisplacements[9]),\
                                             (self.x6, nodesCoordinatesNum[5, 0]), (self.y6, nodesCoordinatesNum[5, 1]),\
                                             (self.nodalDisplacements[10], nodalDisplacements[10]), (self.nodalDisplacements[11], nodalDisplacements[11]),\
                                             (self.x7, nodesCoordinatesNum[6, 0]), (self.y7, nodesCoordinatesNum[6, 1]),\
                                             (self.nodalDisplacements[12], nodalDisplacements[12]), (self.nodalDisplacements[13], nodalDisplacements[13]),\
                                             (self.x8, nodesCoordinatesNum[7, 0]), (self.y8, nodesCoordinatesNum[7, 1]),\
                                             (self.nodalDisplacements[14], nodalDisplacements[14]), (self.nodalDisplacements[15], nodalDisplacements[15])])
                tempExprStrainsNum = sp.lambdify([self.xi, self.eta], tempExprStrains, 'numpy')
            
                tempExprStresses = self.stresses.subs([(self.Ee, EeNum), (self.nu, nuNum),\
                                                       (self.x1, nodesCoordinatesNum[0, 0]), (self.y1, nodesCoordinatesNum[0, 1]),\
                                             (self.nodalDisplacements[0], nodalDisplacements[0]), (self.nodalDisplacements[1], nodalDisplacements[1]),\
                                             (self.x2, nodesCoordinatesNum[1, 0]), (self.y2, nodesCoordinatesNum[1, 1]),\
                                             (self.nodalDisplacements[2], nodalDisplacements[2]), (self.nodalDisplacements[3], nodalDisplacements[3]),\
                                             (self.x3, nodesCoordinatesNum[2, 0]), (self.y3, nodesCoordinatesNum[2, 1]),\
                                             (self.nodalDisplacements[4], nodalDisplacements[4]), (self.nodalDisplacements[5], nodalDisplacements[5]),\
                                             (self.x4, nodesCoordinatesNum[3, 0]), (self.y4, nodesCoordinatesNum[3, 1]),\
                                             (self.nodalDisplacements[6], nodalDisplacements[6]), (self.nodalDisplacements[7], nodalDisplacements[7]),\
                                             (self.x5, nodesCoordinatesNum[4, 0]), (self.y5, nodesCoordinatesNum[4, 1]),\
                                             (self.nodalDisplacements[8], nodalDisplacements[8]), (self.nodalDisplacements[9], nodalDisplacements[9]),\
                                             (self.x6, nodesCoordinatesNum[5, 0]), (self.y6, nodesCoordinatesNum[5, 1]),\
                                             (self.nodalDisplacements[10], nodalDisplacements[10]), (self.nodalDisplacements[11], nodalDisplacements[11]),\
                                             (self.x7, nodesCoordinatesNum[6, 0]), (self.y7, nodesCoordinatesNum[6, 1]),\
                                             (self.nodalDisplacements[12], nodalDisplacements[12]), (self.nodalDisplacements[13], nodalDisplacements[13]),\
                                             (self.x8, nodesCoordinatesNum[7, 0]), (self.y8, nodesCoordinatesNum[7, 1]),\
                                             (self.nodalDisplacements[14], nodalDisplacements[14]), (self.nodalDisplacements[15], nodalDisplacements[15])])
                tempExprStressesNum = sp.lambdify([self.xi, self.eta], tempExprStresses, 'numpy')
        
        # Generates strains' and stresses' output data at Gauss points.
        if(outputAt.count('GP') > 0):
            if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Evaluating strains and stresses at Gauss points and preparing output for the external program.\n')
            if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'Evaluating strains and stresses at GPs and preparing output for the external program.')
                
            gpNum = np.array(self.gp.tolist()).astype(np.float64)
            
            for i in range(0, self.gp.shape[0]):
                matTemp = tempExprStrainsNum(gpNum[i, 0], gpNum[i, 1])
                strains.append('eps_x (GP ' + str(i + 1) + ') = ' + str(matTemp[0,0]))
                strains.append('eps_y (GP ' + str(i + 1) + ') = ' + str(matTemp[1,0]))
                # Output data for shear strains will be generated for each node only if reduced integration of shear terms is not used.
                if(self.reducedIntegrationShear == False):
                    strains.append('eps_xy (GP ' + str(i + 1) + ') = ' + str(matTemp[2,0]))

                matTemp = tempExprStressesNum(gpNum[i, 0], gpNum[i, 1])
                stresses.append('sig_x (GP ' + str(i + 1) + ') = ' + str(matTemp[0,0]))
                stresses.append('sig_y (GP ' + str(i + 1) + ') = ' + str(matTemp[1,0]))
                # Output data for shear stresses will be generated for each node only if reduced integration of shear terms is not used.
                if(self.reducedIntegrationShear == False):
                    stresses.append('sig_xy (GP ' + str(i + 1) + ') = ' + str(matTemp[2,0]))
        
        # Generates strains' and stresses' output data at nodes.
        if(outputAt.count('N') > 0):
            if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Evaluating strains and stresses at nodes and preparing output for the external program.\n')
            if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'Evaluating strains and stresses at nodes and preparing output for the external program.')
            
            for i in range(0, self.numberOfNodes):
                matTemp = tempExprStrainsNum(self.nodesCoordinatesParametric[i, 0], self.nodesCoordinatesParametric[i, 1])
                strains.append('eps_x (Node ' + str(nodesCoordinatesNum[i, 0]) + ', ' + str(nodesCoordinatesNum[i, 1]) + ') = ' + str(matTemp[0,0]))
                strains.append('eps_y (Node ' + str(nodesCoordinatesNum[i, 0]) + ', ' + str(nodesCoordinatesNum[i, 1]) + ') = ' + str(matTemp[1,0]))
                # Output data for shear strains will be generated for each node only if reduced integration of shear terms is not used.
                if(self.reducedIntegrationShear == False):
                    strains.append('eps_xy (Node ' + str(nodesCoordinatesNum[i, 0]) + ', ' + str(nodesCoordinatesNum[i, 1]) + ') = ' + str(matTemp[2,0]))
                
                matTemp = tempExprStressesNum(self.nodesCoordinatesParametric[i, 0], self.nodesCoordinatesParametric[i, 1])
                stresses.append('sig_x (Node ' + str(nodesCoordinatesNum[i, 0]) + ', ' + str(nodesCoordinatesNum[i, 1]) + ') = ' + str(matTemp[0,0]))
                stresses.append('sig_y (Node ' + str(nodesCoordinatesNum[i, 0]) + ', ' + str(nodesCoordinatesNum[i, 1]) + ') = ' + str(matTemp[1,0]))
                if(self.reducedIntegrationShear == False):
                    stresses.append('sig_xy (Node ' + str(nodesCoordinatesNum[i, 0]) + ', ' + str(nodesCoordinatesNum[i, 1]) + ') = ' + str(matTemp[2,0]))
        
        # Generates output data for shear strains and stresses when reduced integration of shear terms is used (at element's centre).                      
        # This will not be executed in case when recovery of strains and stresses at the element's centre is required because
        # it produces the same data for shear terms as the following block of code.
        if(self.reducedIntegrationShear and outputAt.count('C') == 0):
                matTemp = tempExprStrainsNum(0, 0)
                strains.append('eps_xy = ' + str(matTemp[2, 0]))
            
                matTemp = tempExprStressesNum(0, 0)
                stresses.append('sig_xy = ' + str(matTemp[2, 0]))
        
        # Generates strains' and stresses' output data at element's centre.
        # The following block is placed after the block that is executed when reduced integration of shear terms is employed
        # because recovery of all components pf strains and stresses at the element's centre includes shear terms as well.
        if(outputAt.count('C') > 0):
            if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
                with open(self.analysisDetailsFileName, 'a') as outputFile:
                    outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Evaluating strains and stresses at the centre and preparing output for the external program.\n')
            if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
                print(dt.datetime.now().strftime('%x %X') + '\t' + 'Evaluating strains and stresses at the centre and preparing output for the external program.')
            
            matTemp = tempExprStrainsNum(0, 0)
            strains.append('eps_x (at element\'s centre) = ' + str(matTemp[0,0]))
            strains.append('eps_y (at element\'s centre) = ' + str(matTemp[1,0]))
            strains.append('eps_xy (at element\'s centre) = ' + str(matTemp[2,0]))
                
            matTemp = tempExprStressesNum(0, 0)
            stresses.append('sig_x (at element\'s centre) = ' + str(matTemp[0,0]))
            stresses.append('sig_y (at element\'s centre) = ' + str(matTemp[1,0]))
            stresses.append('sig_xy (at element\'s centre) = ' + str(matTemp[2,0]))
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Strains and stresses evaluated and handed over to an external program.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Strains and stresses evaluated and handed over to an external program.')
        
        return strains + stresses
    
    
    def calculateSurfaceLoadDistribution(self, giveExamples = False):
        px = sp.symbols('p_x')
        py = sp.symbols('p_y')
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating load distribution.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating load distribution.')
        
        if(self.numberOfGPs == 4):
            nodalLoadsTemp = self.jacobianDeterminant.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])+\
                                                                self.jacobianDeterminant.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])+\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])+\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])])
        elif(self.numberOfGPs == 9):
            nodalLoadsTemp = self.jacobianDeterminant.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])*self.wf[0, 0] +\
                                                                self.jacobianDeterminant.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])*self.wf[1, 0] +\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])*self.wf[2, 0] +\
                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])])*self.wf[3, 0] +\
                                                                        self.jacobianDeterminant.subs([(self.xi,self.gp[4, 0]),(self.eta,self.gp[4, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[4, 0]),(self.eta,self.gp[4, 1])])*self.wf[4, 0] +\
                                                                            self.jacobianDeterminant.subs([(self.xi,self.gp[5, 0]),(self.eta,self.gp[5, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[5, 0]),(self.eta,self.gp[5, 1])])*self.wf[5, 0] +\
                                                                                self.jacobianDeterminant.subs([(self.xi,self.gp[6, 0]),(self.eta,self.gp[6, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[6, 0]),(self.eta,self.gp[6, 1])])*self.wf[6, 0] +\
                                                                                    self.jacobianDeterminant.subs([(self.xi,self.gp[7, 0]),(self.eta,self.gp[7, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[7, 0]),(self.eta,self.gp[7, 1])])*self.wf[7, 0] +\
                                                                                        self.jacobianDeterminant.subs([(self.xi,self.gp[8, 0]),(self.eta,self.gp[8, 1])])*self.shapeFunction.T.subs([(self.xi,self.gp[8, 0]),(self.eta,self.gp[8, 1])])*self.wf[8, 0]
        
        j = 0
        for i in range(0, nodalLoadsTemp.shape[0]):
            self.nodalLoads[j, 0] = px * nodalLoadsTemp[i, 0]
            self.nodalLoads[j + 1, 0] = py * nodalLoadsTemp[i, 0]
            j+=2
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Load distribution calculated.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Load distribution calculated.')
        
        if(self.latexOutput):
            if(self.nodalLoads.shape[0] == 8):
                Fi = sp.Matrix([[sp.symbols('F_{x1}')],[sp.symbols('F_{y1}')],[sp.symbols('F_{x2}')],[sp.symbols('F_{y2}')],[sp.symbols('F_{x3}')],[sp.symbols('F_{y3}')],[sp.symbols('F_{x4}')],[sp.symbols('F_{y4}')]])
            elif(self.nodalLoads.shape[0] == 16):
                Fi = sp.Matrix([[sp.symbols('F_{x1}')],[sp.symbols('F_{y1}')],[sp.symbols('F_{x2}')],[sp.symbols('F_{y2}')],[sp.symbols('F_{x3}')],[sp.symbols('F_{y3}')],[sp.symbols('F_{x4}')],[sp.symbols('F_{y4}')],[sp.symbols('F_{x5}')],[sp.symbols('F_{y5}')],[sp.symbols('F_{x6}')],[sp.symbols('F_{y6}')],[sp.symbols('F_{x7}')],[sp.symbols('F_{y7}')],[sp.symbols('F_{x8}')],[sp.symbols('F_{y8}')]])
            self.latexOut.addSubsection('Load distribution')
            self.latexOut.addText('Constant surface loads in x direction ($p_x$) and in y direction ($p_y$) are assumed.')
            self.latexOut.addMath(sp.latex(Fi) + ' = ' + sp.latex(self.nodalLoads))
            
        if(giveExamples):
            x1Num = -1
            y1Num = -1
            x3Num = 1
            y3Num = -1
            
            if(nodalLoadsTemp.shape[0] == 16):
                x2Num = (x1Num + x3Num)/2
                y2Num = (y1Num + y3Num)/2
            
            x5Num = 1
            y5Num = 1
            
            if(nodalLoadsTemp.shape[0] == 16):
                x4Num = (x5Num + x3Num)/2
                y4Num = (y5Num + y3Num)/2
            
            x7Num = -1
            y7Num = 1
            
            if(nodalLoadsTemp.shape[0] == 16):
                x6Num = (x5Num + x7Num)/2
                y6Num = (y5Num + y7Num)/2
                x8Num = (x1Num + x7Num)/2
                y8Num = (y1Num + y7Num)/2
            
            p = 10
            
            if(nodalLoadsTemp.shape[0] == 4):
                matTemp = self.nodalLoads.subs([(px, p), (py, 0),\
                                             (self.x1, x1Num), (self.y1, y1Num),\
                                                 (self.x2, x3Num), (self.y2, y3Num),\
                                                     (self.x3, x5Num), (self.y3, y5Num),\
                                                         (self.x4, x7Num), (self.y4, y7Num)])
            elif(nodalLoadsTemp.shape[0] == 8):
                matTemp = self.nodalLoads.subs([(px, p), (py, 0),\
                                             (self.x1, x1Num), (self.y1, y1Num),\
                                                 (self.x2, x2Num), (self.y2, y2Num),\
                                                     (self.x3, x3Num), (self.y3, y3Num),\
                                                         (self.x4, x4Num), (self.y4, y4Num),\
                                                               (self.x5, x5Num), (self.y5, y5Num),\
                                                                   (self.x6, x6Num), (self.y6, y6Num),\
                                                                       (self.x7, x7Num), (self.y7, y7Num),\
                                                                           (self.x8, x8Num), (self.y8, y8Num)])
        
            matTempEvaluated = matTemp.evalf()
            if(self.latexOutput):
                self.latexOut.addText(r'Example: Square\\Constant surface load: $p_x = 10$\\$F_{xn}$ is a nodal force in x direction at '+\
                                      'node n; $F_{yn}$ is a nodal force in y direction at node n.')
                self.latexOut.addMath(sp.latex(Fi) + ' = ' + sp.latex(matTempEvaluated))
            
            x1Num = -1.5
            y1Num = -1
            x2Num = 1.5
            y2Num = -1
            x3Num = 1.5
            y3Num = 1
            x4Num = -1.5
            y4Num = 1
            p = 10
            
            matTemp = self.nodalLoads.subs([(px, p), (py, 0),\
                                             (self.x1, x1Num), (self.y1, y1Num),\
                                                 (self.x2, x2Num), (self.y2, y2Num),\
                                                     (self.x3, x3Num), (self.y3, y3Num),\
                                                         (self.x4, x4Num), (self.y4, y4Num)])
        
            matTempEvaluated = matTemp.evalf()
            if(self.latexOutput):
                self.latexOut.addText(r'Example: Rectangle\\Constant surface load: $p_x = 10$\\$F_{xn}$ is a nodal force in x direction at '+\
                                      'node n; $F_{yn}$ is a nodal force in y direction at node n.')
                self.latexOut.addMath(sp.latex(Fi) + ' = ' + sp.latex(matTempEvaluated))
            
            x1Num = -1.5
            y1Num = -1
            x2Num = 0.5
            y2Num = -1
            x3Num = 1.5
            y3Num = 1
            x4Num = -0.5
            y4Num = 1
            p = 10
            
            matTemp = self.nodalLoads.subs([(px, p), (py, 0),\
                                             (self.x1, x1Num), (self.y1, y1Num),\
                                                 (self.x2, x2Num), (self.y2, y2Num),\
                                                     (self.x3, x3Num), (self.y3, y3Num),\
                                                         (self.x4, x4Num), (self.y4, y4Num)])
        
            matTempEvaluated = matTemp.evalf()
            if(self.latexOutput):
                self.latexOut.addText(r'Example: Parallelogram\\Constant surface load: $p_x = 10$\\$F_{xn}$ is a nodal force in x direction at '+\
                                      'node n; $F_{yn}$ is a nodal force in y direction at node n.')
                self.latexOut.addMath(sp.latex(Fi) + ' = ' + sp.latex(matTempEvaluated))
            
            x1Num = -1.5
            y1Num = -1
            x2Num = 1.5
            y2Num = -1
            x3Num = 0.5
            y3Num = 1
            x4Num = -0.5
            y4Num = 1
            p = 10
            
            matTemp = self.nodalLoads.subs([(px, p), (py, 0),\
                                             (self.x1, x1Num), (self.y1, y1Num),\
                                                 (self.x2, x2Num), (self.y2, y2Num),\
                                                     (self.x3, x3Num), (self.y3, y3Num),\
                                                         (self.x4, x4Num), (self.y4, y4Num)])
        
            matTempEvaluated = matTemp.evalf()
            if(self.latexOutput):
                self.latexOut.addText(r'Example: Trapezium\\Constant surface load: $p_x = 10$\\$F_{xn}$ is a nodal force in x direction at '+\
                                      'node n; $F_{yn}$ is a nodal force in y direction at node n.')
                self.latexOut.addMath(sp.latex(Fi) + ' = ' + sp.latex(matTempEvaluated))
            
            x1Num = -1.5
            y1Num = -1
            x2Num = 1
            y2Num = -0.5
            x3Num = 0.5
            y3Num = 1.5
            x4Num = -0.5
            y4Num = 1
            p = 10
            
            matTemp = self.nodalLoads.subs([(px, p), (py, 0),\
                                             (self.x1, x1Num), (self.y1, y1Num),\
                                                 (self.x2, x2Num), (self.y2, y2Num),\
                                                     (self.x3, x3Num), (self.y3, y3Num),\
                                                         (self.x4, x4Num), (self.y4, y4Num)])
        
            matTempEvaluated = matTemp.evalf()
            if(self.latexOutput):
                self.latexOut.addText(r'Example: General quadrilateral\\Constant surface load: $p_x = 10$\\$F_{xn}$ is a nodal force in x direction at '+\
                                      'node n; $F_{yn}$ is a nodal force in y direction at node n.')
                self.latexOut.addMath(sp.latex(Fi) + ' = ' + sp.latex(matTempEvaluated))
        return
