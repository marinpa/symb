# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 17:13:48 2021

@author: mpalaversa
"""
import datetime as dt
import numpy as np
import sympy as sp

import fes
import plnstrsselement as pse

# The basic plane stress quadrialteral finite element
class PlnStrssQd1(pse.PlnStrssElement):
    """
    4-node plane stress quadrialteral finite element.
    """
    def __init__(self, saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis = False):
        try:
            self.name
        except AttributeError:
            self.name = 'PlnStrssQd1'
            
        super().__init__(saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis)
        
        self.nodesCoordinatesParametric = np.zeros([4, 2], dtype = int)
        self.shapeFunction = sp.zeros(1, 4)
        self.nodalDisplacements = sp.zeros(8, 1)
        self.BMatrix = sp.zeros(3, 8)
        self.numberOfGPs = 4
        self.gp = sp.zeros(4, 2)
        self.stiffnessMatrix = sp.zeros(8, 8)
        self.nodalLoads = sp.zeros(8, 1)
        
        if(numericalAnalysis):
            self.stiffnessMatrixNum = np.empty([8, 8])
            self.numOfDofsPerNode = len(fes.getFE(self.name))
        
        self.numberOfNodes = 4
        self.isInitialised = False
        pass
        
    def initialiseElement(self):
        self.setCommonVariables()
        self.setElementVertices()
        
        self.nodalDisplacements[0, 0], self.nodalDisplacements[1, 0], self.nodalDisplacements[2, 0], self.nodalDisplacements[3, 0], \
            self.nodalDisplacements[4, 0], self.nodalDisplacements[5, 0], self.nodalDisplacements[6, 0], self.nodalDisplacements[7, 0] =\
            sp.symbols('u_1 v_1 u_2 v_2 u_3 v_3 u_4 v_4')
        
        # Coordinates of nodes in parametric space
        self.nodesCoordinatesParametric[0, 0] = -1
        self.nodesCoordinatesParametric[0, 1] = -1
        self.nodesCoordinatesParametric[1, 0] = 1
        self.nodesCoordinatesParametric[1, 1] = -1
        self.nodesCoordinatesParametric[2, 0] = 1
        self.nodesCoordinatesParametric[2, 1] = 1
        self.nodesCoordinatesParametric[3, 0] = -1
        self.nodesCoordinatesParametric[3, 1] = 1
        
        self.isInitialised = True
        return
        
    def setElementVertices(self):
        # Element vertices in parametric and in metric space:
        self.xi1, self.eta1, self.xi2, self.eta2, self.xi3, self.eta3, self.xi4, self.eta4 =\
            sp.symbols(r"\xi_1,\eta_1,\xi_2,\eta_2,\xi_3,\eta_3,\xi_4,\eta_4")
        self.x1, self.y1, self.x2, self.y2, self.x3, self.y3, self.x4, self.y4 =\
            sp.symbols("x_1,y_1,x_2,y_2,x_3,y_3,x_4,y_4")
        
        if(self.latexOutput):
            self.latexOut.addSubsection('Nodes\' position')
            self.latexOut.addText('In parametric space:')
            self.latexOut.addMath(sp.latex(self.xi1) + ', ' + sp.latex(self.eta1) + ', ' +\
                                  sp.latex(self.xi2) + ', ' + sp.latex(self.eta2) + ', ' +\
                                      sp.latex(self.xi3) + ', ' + sp.latex(self.eta3) + ', ' +\
                                          sp.latex(self.xi4) + ', ' + sp.latex(self.eta4))
            
            self.latexOut.addText('In metric space:')
            self.latexOut.addMath(sp.latex(self.x1) + ', ' + sp.latex(self.y1) + ', ' +\
                                  sp.latex(self.x2) + ', ' + sp.latex(self.y2) + ', ' +\
                                      sp.latex(self.x3) + ', ' + sp.latex(self.y3) + ', ' +\
                                          sp.latex(self.x4) + ', ' + sp.latex(self.y4))
        return
    
    def calculateShapeFunction(self):
        if(self.isInitialised == False):
            self.initialiseElement()
            
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating shape functions.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating shape functions.')
        
        X_1j = sp.Matrix([[1,self.xi,self.eta,self.xi*self.eta]])
        
        if(self.latexOutput):
            self.latexOut.addSubsection('Position/displacement basis function (based on isoparametric FE assumption)')
            self.latexOut.addMath(r'\left[X\right] = ' + sp.latex(X_1j))
        
        self.x_i = sp.Matrix([[self.x1],[self.y1],[self.x2],[self.y2],[self.x3],[self.y3],[self.x4],[self.y4]])
        
        if(self.latexOutput):
            self.latexOut.addSubsection('Vector of nodal coordinates (in metric space):')
            self.latexOut.addMath(r'\left\{x_N\right\} = ' + sp.latex(self.x_i))

        X_ij = sp.Matrix([X_1j.subs([(self.xi,self.xi1),(self.eta,self.eta1)]),\
                           X_1j.subs([(self.xi,self.xi2),(self.eta,self.eta2)]),\
                               X_1j.subs([(self.xi,self.xi3),(self.eta,self.eta3)]),\
                               X_1j.subs([(self.xi,self.xi4),(self.eta,self.eta4)])])
        
        X_ij_subst = X_ij.subs([(self.xi1,self.nodesCoordinatesParametric[0, 0]),(self.eta1,self.nodesCoordinatesParametric[0, 1]),\
                                              (self.xi2,self.nodesCoordinatesParametric[1, 0]),(self.eta2,self.nodesCoordinatesParametric[1, 1]),\
                                                  (self.xi3,self.nodesCoordinatesParametric[2, 0]),(self.eta3,self.nodesCoordinatesParametric[2, 1]),\
                                                      (self.xi4,self.nodesCoordinatesParametric[3, 0]),(self.eta4,self.nodesCoordinatesParametric[3, 1])])
            
        if(self.latexOutput):
            self.latexOut.addSubsection('Matrix of constants:')
            self.latexOut.addMath(r'\left[C\right] = ' + sp.latex(X_ij) + ' = ' + sp.latex(X_ij_subst))
        
        self.shapeFunction = X_1j*X_ij_subst**-1
        
        if(self.latexOutput):
            self.latexOut.addSubsection('Displacement shape functions:')
            self.latexOut.addMath(r'\left[N\right] = ' + sp.latex(self.shapeFunction))
            
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Shape functions calculated.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Shape functions calculated.')
            
        return
    
    def get_element_stiff_matrix(self, nodesCoordinatesList, eldata):
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' +\
                                 'Importing coordinates of the nodes, Young\'s modulus and Poisson\'s coefficient from an external program. Calculating stiffness matrix evaluated with the imported data.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Importing data from an external program and evaluating stiffness matrix.')
        
        EeNum = eldata[0]
        nuNum = eldata[1]
        tNum = eldata[2]
        
        nodesCoordinatesNum = np.empty([4, 2])
        nodesCoordinatesNum[0] = nodesCoordinatesList[0]
        nodesCoordinatesNum[1] = nodesCoordinatesList[1]
        nodesCoordinatesNum[2] = nodesCoordinatesList[2]
        nodesCoordinatesNum[3] = nodesCoordinatesList[3]
        
        if(self.isMatrixEmpty(self.stiffnessMatrix)):
            self.calculateStiffnessMatrix()
        
        matTemp = self.stiffnessMatrix.subs([(self.Ee, EeNum), (self.nu, nuNum), (self.t, tNum),\
                                             (self.x1, nodesCoordinatesNum[0, 0]), (self.y1, nodesCoordinatesNum[0, 1]),\
                                                 (self.x2, nodesCoordinatesNum[1, 0]), (self.y2, nodesCoordinatesNum[1, 1]),\
                                                     (self.x3, nodesCoordinatesNum[2, 0]), (self.y3, nodesCoordinatesNum[2, 1]),\
                                                         (self.x4, nodesCoordinatesNum[3, 0]), (self.y4, nodesCoordinatesNum[3, 1])])
        
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' +\
                                 'Evaluating stiffness matrix for current element.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Evaluating stiffness matrix for current element.')
        
        matTempEvaluated = matTemp.evalf()
        self.stiffnessMatrixNum = np.array(matTempEvaluated.tolist()).astype(np.float64)
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix evaluated at nodes given by an external program together with Young\'s modulus and Poisson\'s coefficient. Stiffness matrix handed over to the external code.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix evaluated as required by an external program and handed over to it.')
        
        return self.stiffnessMatrixNum
    
    def get_node_dofs(self):
        return fes.getFE(self.name)
