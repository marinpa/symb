from typing import List
import numpy as np
import os
import plnstrssqd1 as psq1
import plnstrssqd1red1 as psq1red1
import plnstrssqd2 as psq2
import plnstrssqd2red1 as psq2red1
import plnstrssqd2red1rot2 as psq2red1rot2
import sys
# region General helper functions
def _get_trans_matrix_plate_base(nodes:List[np.ndarray])->np.ndarray:
    #Kelly, PA. Mechanics Lecture Notes: An introduction to Solid Mechanics.
    #Available from http://homepages.engineering.auckland.ac.nz/~pkel015/SolidMechanicsBooks/index.html
    # https://math.stackexchange.com/questions/2004800/math-for-simple-3d-coordinate-rotation-python

    # 1. determine new axes
    new_xaxis = nodes[1]-nodes[0]
    vec23=nodes[2]-nodes[1]
    new_zaxis =np.cross(new_xaxis,vec23)

    new_xaxis = new_xaxis/np.linalg.norm(new_xaxis)
    new_zaxis = new_zaxis / np.linalg.norm(new_zaxis)
    new_yaxis = -np.cross(new_xaxis, new_zaxis)

    #print('new x:', new_xaxis)
    #print('new y:', new_yaxis)
    #print('new z:', new_zaxis)

    new_axes = np.array([new_xaxis, new_yaxis, new_zaxis])
    #print('New axes norms',[np.linalg.norm(new_xaxis),np.linalg.norm(new_yaxis),np.linalg.norm(new_zaxis)])
    # 2. set new axes
    old_axes = np.array([1, 0, 0, 0, 1, 0, 0, 0, 1], dtype=float).reshape(3, -1)
    #print(old_axes)
    # 3. get inner product
    rotation_matrix = np.inner(new_axes, old_axes)
    return rotation_matrix.transpose()

def _get_edge_lengths(nodes:List[np.ndarray]):
    lens=[]
    for i in range(len(nodes)-1):
        lens.append(np.linalg.norm(nodes[i+1]-nodes[i]))
    lens.append(np.linalg.norm(nodes[0] - nodes[len(nodes)-1]))
    return lens

def _get_trans_matrix_plate(nodes:List[np.ndarray],dofs:List[int])->np.ndarray:
    ndof = len(dofs)
    tbdofs =[0]*ndof
    for idof in range(ndof):
        if dofs[idof] > 2:
            tbdofs[idof] = dofs[idof]-3
        else:
            tbdofs[idof] = dofs[idof]
    Tbase = _get_trans_matrix_plate_base(nodes)
    T = np.ndarray((ndof, ndof))
    T[dofs,dofs] = Tbase[tbdofs,tbdofs]
    return T
# endregion  General helper functions

# region Element specific functions
class Element():
    #static members
    elements = {} # Dictinary with symbolicaly derived elements
    def __init__(self,eltype:str, nodes:List[np.ndarray],eldata:List[float],usetrans:bool):
        self._eldata=eldata
        self._nodes = nodes
        self._femelement = None
        self._usetrans = usetrans
        if eltype in Element.elements:
            self._femelement = Element.elements[eltype]
        else:
            if eltype == 'PlnStrssQd1':
                # Argumenti u donjoj metodi: prvi se odnosi na generiranje vanjske datoteke s podacima o proračunu, drugi na ispis podataka o proračunu u Python-konzolu,
                # treći se odnosi na nivo informacija koje se ispisuju (može biti D za debug ili U za user; debug je detaljniji), četvrti se odnosi na učitavanje međurezultata
                # proračuna iz vanjske datoteke (ako postoje) i zadnji se odnosi na numeričku analizu (ovdje uvijek mora biti True).
                self._femelement = psq1.PlnStrssQd1(False, True, 'U', True, True)
            elif eltype == 'PlnStrssQd1Red1':
                self._femelement = psq1red1.PlnStrssQd1Red1(False, True, 'U', True, True)
            elif eltype == 'PlnStrssQd2':
                self._femelement = psq2.PlnStrssQd2(False, True, 'U', True, True)
            elif eltype == 'PlnStrssQd2Red1':
                self._femelement = psq2red1.PlnStrssQd2Red1(False, True, 'U', True, True)
            elif eltype == 'PlnStrssQd2Red1Rot2':
                self._femelement = psq2red1rot2.PlnStrssQd2Red1Rot2(False, True, 'U', True, True)
            else:
                print('Error, Unkonwn element type! Program execution terminated!')
                sys.exit()
            
            # Donja metoda uključuje opciju spremanja rezultata potencijalno dugih proračuna u vanjsku datoteku. Rezultat će se spremiti ako je vrijeme potrebno za njegovo dobivanje veće od onog
            # koje je dano brojem u listi argumenata.
            #self._femelement.setDataStoringOptions(True, 60)
            # Donja metoda uključuje generiranje Latex-ispisa. Prvi argument je path do izlaznog filea, drugi je tip dokumenta, treći i četvrti su format i orijentacija papira, peti je veličina fonta,
            # šesti je jezikajezik, a zadnja dva su naslov i autor (zadnja dva se ne moraju zadati te se u tom slučaju neće generirati naslovna strana).
            #self._femelement.generateLatexOutput(os.path.realpath('.') + '\\latex_output\\quad8', 'article', 'a3', 'landscape', '12', 'english', 'PlnStrssQd2', 'Marin Palaversa')
            Element.elements[eltype] = self._femelement

    def get_node_transformation_matrix(self)->np.ndarray:
        dofs = self.get_node_dofs()
        return _get_trans_matrix_plate(self._nodes,dofs) # ovo bi trebalo biti dobro za sve vrste 2D elemenata

    def get_node_dofs(self)->List[int]:
        #  tx ty tz rx ry rz
        #[ 0, 1, 2, 3, 4, 5 ]
        # e.g  return [ 0, 1] specifiy that there are two dofs per node, namely tx and ty
        return self._femelement.get_node_dofs()

    def get_num_nodes(self):
        return len(self._nodes)

    def get_num_node_dofs(self):
        """
        Get number of dofs per node
        :return:
        """
        return self._femelement.numOfDofsPerNode

    def get_num_element_dofs(self):
        return self.get_num_nodes()*self.get_num_node_dofs()

    def get_element_transformation_matrix(self)->np.ndarray:
        ndofel = self.get_num_element_dofs()
        ndof = self.get_num_node_dofs()
        Tnod = self.get_node_transformation_matrix()
        Tel = np.zeros((ndofel, ndofel))
        inod = 0
        for nod in self._nodes:
            Tel[inod * ndof:(inod + 1) * ndof, inod * ndof:(inod + 1) * ndof] = Tnod
            inod += 1
        return Tel
    def get_nodes_in_local(self ,num_axis=3)->List[np.ndarray]:
        """

        :param num_axis: number of axis (if 2 only x,y cord wil be in array), default = 3
        :return: List of element nodes with coordinates in local coordinate system
        """
        nodesloc = []
        if self._usetrans:
            TTbase = (_get_trans_matrix_plate_base(self._nodes)).transpose() # 3x3 matrix
            for nod in self._nodes:
                nodloc = TTbase @ nod
                nodesloc.append(nodloc[0:num_axis])
        else:
            for nod in self._nodes:
                nodesloc.append(nod[0:num_axis])
        return nodesloc

    def get_element_stiff_matrix(self)->np.ndarray:
        """
        get_element_stiff_matrix
        :param eltype: element type
        :param nodes: nodes containing x,y,z coordinates in global coordinate system
        :param eldata:
        :return:
        """
        #Ako je 2D element
        E = self._eldata[0]
        ni = self._eldata[1]
        tp = self._eldata[2]

        nnod= self.get_num_nodes()
        ndof = self.get_num_node_dofs()
        ndofel = self.get_num_element_dofs()
        # transform nodes to local coordinate system
        locnodes = self.get_nodes_in_local(2) # if argument is 2, each node will have only x and y
        #x1=locnodes[0][0]
        #y1 = locnodes[0][1]
        #z1 = locnodes[0][2]
        # prepare local matrix
        #kloc = np.identity(ndofel)
        kloc = self._femelement.get_element_stiff_matrix(locnodes,self._eldata)
        # transform to stifness matrix in global coordinate system
        if not self._usetrans:
            return kloc
        T = self.get_element_transformation_matrix()
        TT = T.transpose()
        kglob = TT @ kloc @ T
        return kloc # Test without transformation
        return kglob

    def get_element_results(self,disp:List[np.ndarray]) -> List[str]:
        if self._usetrans:
            TTnod = (self.get_node_transformation_matrix()).transpose()
            locdisp = TTnod @ disp
        else:
            locdisp = disp
        locnodes = self.get_nodes_in_local(2)  # if argument is 2, each node will have only x and y
        results = self._femelement.get_strains_and_stresses(locnodes,locdisp, self._eldata, ['GP', 'N', 'C'])
        return  results
# endregion Element specific functions
