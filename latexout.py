# -*- coding: utf-8 -*-
"""
Created on Fri Feb 19 11:26:39 2021

@author: mposao
"""

class LatexOut:
    def __init__(self, fn, docType, paperSize, orientation, fontSize, language, title = '', author = ''):
        self.fileName = fn
        
        if('.tex' in self.fileName):
            self.file = open(self.fileName, 'w')
        else:
            self.fileName = self.fileName + '.tex'
            self.file = open(self.fileName, 'w')
        
        if(paperSize == 'a4' and orientation == 'portrait'):
            self.file.write('\documentclass[' + paperSize + 'paper,' + fontSize + 'pt]{' +
                            docType + r'}' + '\n' + r'\usepackage[' + language + ']{babel}\n' +
                            r'\usepackage{amsmath,times}' + '\n' + r'\usepackage{graphicx}' + '\n' +
                            r'\usepackage{pdflscape}' + '\n' + r'\title{' + title + '}\n' +
                            r'\author{' + author + '}\n' + r'\begin{document}' + '\n')
        elif(paperSize == 'a3' and orientation == 'landscape'):
                self.file.write('\documentclass[' + fontSize + 'pt]{' +
                                docType + r'}' + '\n' + r'\usepackage[landscape,' + paperSize + 'paper]{geometry}\n' +
                                r'\usepackage[' + language + ']{babel}\n' + r'\usepackage{amsmath,times}' + '\n' +
                                r'\usepackage{graphicx}' + '\n' + r'\usepackage{pdflscape}' + '\n' +
                                r'\title{' + title + '}\n' + r'\author{' + author + '}\n' + r'\begin{document}' + '\n')
        
        if(title != ''):
            self.file.write(r'\begin{titlepage}' + '\n\maketitle\n' + r'\thispagestyle{empty}' + '\n' +
                            '\end{titlepage}\n')
        
        self.file.close()
        
    def addSection(self, sectionTitle):
        if(self.file.closed):
            self.file = open(self.fileName, 'a')
        self.file.write('\section{' + sectionTitle + '}\n')
    
    def addSubsection(self, subsectionTitle):
        if(self.file.closed):
            self.file = open(self.fileName, 'a')
        self.file
        self.file.write('\subsection{' + subsectionTitle + '}\n')
        
    def addText(self, text):
        if(self.file.closed):
            self.file = open(self.fileName, 'a')
        self.file.write(text + '\\\ \n')
        
    def addMath(self, math):
        if(self.file.closed):
            self.file = open(self.fileName, 'a')
        self.file.write('$\n\displaystyle ' + math + '\n$ \\\ \n')
        
    def finish(self):
        if(self.file.closed):
            self.file = open(self.fileName, 'a')
        self.file.write('\end{document}')
        self.file.close()