# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 13:13:26 2021

@author: mpalaversa
"""
import numpy as np
import sympy as sp

import fes
import plnstrssqd2red1 as psq2red1

class PlnStrssQd2Rot2(psq2red1.PlnStrssQd2Red1):
    """
    4-node plane stress quadrialteral finite element with rotational DOFs.
    """
    def __init__(self, saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis = False):
        self.name = 'PlnStrssQd2Rot2'
        
        super().__init__(saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis)
        
        if(numericalAnalysis):
            self.nodalDisplacementsNum = np.empty([12, 1])
            self.stiffnessMatrixNum = np.empty([12, 12])
            
            self.numOfDofsPerNode = len(fes.getFE(self.name))
        
        pass
    
    def getTransformationMatrix(self):
        self.transformationMatrix = sp.zeros(16, 12)
        self.transformationMatrix[0, 0] = 1
        self.transformationMatrix[1, 1] = 1
        self.transformationMatrix[2, 0] = 1/2
        self.transformationMatrix[2, 2] = -(self.y3 - self.y1)/8
        self.transformationMatrix[2, 3] = 1/2
        self.transformationMatrix[2, 5] = (self.y3 - self.y1)/8
        self.transformationMatrix[3, 1] = 1/2
        self.transformationMatrix[3, 2] = (self.x3 - self.x1)/8
        self.transformationMatrix[3, 4] = 1/2
        self.transformationMatrix[3, 5] = -(self.x3 - self.x1)/8
        self.transformationMatrix[4, 3] = 1
        self.transformationMatrix[5, 4] = 1
        self.transformationMatrix[6, 3] = 1/2
        self.transformationMatrix[6, 5] = -(self.y5 - self.y3)/8
        self.transformationMatrix[6, 6] = 1/2
        self.transformationMatrix[6, 8] = (self.y5 - self.y3)/8
        self.transformationMatrix[7, 4] = 1/2
        self.transformationMatrix[7, 5] = (self.x5 - self.x3)/8
        self.transformationMatrix[7, 7] = 1/2
        self.transformationMatrix[7, 8] = -(self.x5 - self.x3)/8
        self.transformationMatrix[8, 6] = 1
        self.transformationMatrix[9, 7] = 1
        self.transformationMatrix[10, 6] = 1/2
        self.transformationMatrix[10, 8] = -(self.y7 - self.y5)/8
        self.transformationMatrix[10, 9] = 1/2
        self.transformationMatrix[10, 11] = (self.y7 - self.y5)/8
        self.transformationMatrix[11, 7] = 1/2
        self.transformationMatrix[11, 8] = (self.x7 - self.x5)/8
        self.transformationMatrix[11, 10] = 1/2
        self.transformationMatrix[11, 11] = -(self.x7 - self.x5)/8
        self.transformationMatrix[12, 9] = 1
        self.transformationMatrix[13, 10] = 1
        self.transformationMatrix[14, 0] = 1/2
        self.transformationMatrix[14, 2] = (self.y1 - self.y7)/8
        self.transformationMatrix[14, 9] = 1/2
        self.transformationMatrix[14, 10] = -(self.y1 - self.y7)/8
        self.transformationMatrix[15, 1] = 1/2
        self.transformationMatrix[15, 2] = -(self.x1 - self.x7)/8
        self.transformationMatrix[15, 10] = 1/2
        self.transformationMatrix[15, 11] = (self.x1 - self.x7)/8
        return
    
    def reinitialiseElement(self):
        self.nodalDisplacements = sp.zeros(12, 1)
        self.nodalDisplacements[0, 0], self.nodalDisplacements[1, 0], self.nodalDisplacements[2, 0], self.nodalDisplacements[3, 0], \
            self.nodalDisplacements[4, 0], self.nodalDisplacements[5, 0], self.nodalDisplacements[6, 0], self.nodalDisplacements[7, 0], \
                self.nodalDisplacements[8, 0], self.nodalDisplacements[9, 0], self.nodalDisplacements[10, 0], self.nodalDisplacements[11, 0] = \
                    sp.symbols('u_1 v_1 \theta_1 u_3 v_3 \theta_3 u_5 v_5 \theta_5 u_7 v_7 \theta_7')
        return
    
    def recalculateBMatrix(self):
        if(self.isMatrixEmpty(self.BMatrix)):
            self.calculateBMatrix()
            
        self.getTransformationMatrix()
        self.BMatrix = self.BMatrix * self.transformationMatrix
        return
    
    def get_node_dofs(self):
        return fes.getFE(self.name)