# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 17:08:16 2021

@author: mpalaversa
"""
import os
import plnstrssqd1 as psq1
import plnstrssqd1red1 as psq1red1
import plnstrssqd2 as psq2
import plnstrssqd2red1 as psq2red1
import plnstrssqd2red1rot2 as psq2red1rot2

fe = psq2.PlnStrssQd2(False, True, 'U', True, False)
fe.setDataStoringOptions(True, 10)
fe.generateLatexOutput(os.path.realpath('.') + '\\..\\latex_output\\PlnStrssQd2', 'article', 'a3', 'landscape', '12', 'english', 'PlnStrssQd2', 'Marin Palaversa')
fe.calculateStiffnessMatrix()
fe.calculateStrains()
fe.calculateStresses()
fe.calculateSurfaceLoadDistribution(True)
fe.finishLatexOutput()