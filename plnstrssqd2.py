# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 17:18:05 2021

@author: mpalaversa
"""
import sympy as sp

import plnstrssqd2red1 as psq2red1

class PlnStrssQd2(psq2red1.PlnStrssQd2Red1):
    """
    8-node plane stress quadrialteral finite element.
    """
    def __init__(self, saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis = False):
        self.name = 'PlnStrssQd2'
        super().__init__(saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis)
        
        self.numberOfGPs = 9
        self.gp = sp.zeros(9, 2)
        self.wf = sp.zeros(9, 1)
        pass