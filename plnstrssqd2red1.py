# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 10:27:05 2021

@author: mpalaversa
"""
import datetime as dt
import numpy as np
import sympy as sp

import fes
import plnstrsselement as pse

# 8-noded plane stress quadrialteral finite element with reduced integration (of all terms in strain-displacement matrix).
class PlnStrssQd2Red1(pse.PlnStrssElement):
    """
    8-noded plane stress quadrialteral finite element with reduced integration (of all terms in strain-displacement matrix).
    """
    def __init__(self, saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis = False):
        try:
            self.name
        except AttributeError:
            self.name = 'PlnStrssQd2Red1'
            
        super().__init__(saveAnalysisDetails, printAnalysisDetails, analysisDetailsMode, useSavedData, numericalAnalysis)
            
        self.nodesCoordinatesParametric = np.zeros([8, 2], dtype = int)
        self.shapeFunction = sp.zeros(1, 8)
        self.nodalDisplacements = sp.zeros(16, 1)
        self.BMatrix = sp.zeros(3, 16)
        self.numberOfGPs = 4
        self.gp = sp.zeros(4, 2)
        self.stiffnessMatrix = sp.zeros(16, 16)
        self.nodalLoads = sp.zeros(16, 1)
        
        if(useSavedData):
            self.readDataFromFile()
        
        if(numericalAnalysis):
            self.nodesCoordinatesNum = np.empty([8, 2])
            self.nodalDisplacementsNum = np.empty([16, 1])
            self.stiffnessMatrixNum = np.empty([16, 16])
            
            self.numOfDofsPerNode = 2
        
        self.numberOfNodes = 8
        self.isInitialised = False
        pass
    
    def initialiseElement(self):
        self.setCommonVariables()
        self.setElementVertices()
        
        self.nodalDisplacements[0, 0], self.nodalDisplacements[1, 0], self.nodalDisplacements[2, 0], self.nodalDisplacements[3, 0], \
            self.nodalDisplacements[4, 0], self.nodalDisplacements[5, 0], self.nodalDisplacements[6, 0], self.nodalDisplacements[7, 0], \
                self.nodalDisplacements[8, 0], self.nodalDisplacements[9, 0], self.nodalDisplacements[10, 0], self.nodalDisplacements[11, 0], \
                   self.nodalDisplacements[12, 0], self.nodalDisplacements[13, 0], self.nodalDisplacements[14, 0], self.nodalDisplacements[15, 0] = \
                       sp.symbols('u_1 v_1 u_2 v_2 u_3 v_3 u_4 v_4 u_5 v_5 u_6 v_6 u_7 v_7 u_8 v_8')
        
        # Coordinates of nodes in parametric space
        self.nodesCoordinatesParametric[0, 0] = -1
        self.nodesCoordinatesParametric[0, 1] = -1
        self.nodesCoordinatesParametric[1, 0] = 0
        self.nodesCoordinatesParametric[1, 1] = -1
        self.nodesCoordinatesParametric[2, 0] = 1
        self.nodesCoordinatesParametric[2, 1] = -1
        self.nodesCoordinatesParametric[3, 0] = 1
        self.nodesCoordinatesParametric[3, 1] = 0
        self.nodesCoordinatesParametric[4, 0] = 1
        self.nodesCoordinatesParametric[4, 1] = 1
        self.nodesCoordinatesParametric[5, 0] = 0
        self.nodesCoordinatesParametric[5, 1] = 1
        self.nodesCoordinatesParametric[6, 0] = -1
        self.nodesCoordinatesParametric[6, 1] = 1
        self.nodesCoordinatesParametric[7, 0] = -1
        self.nodesCoordinatesParametric[7, 1] = 0
        
        self.isInitialised = True
        return
        
    def setElementVertices(self):
        # Element vertices in parametric and in metric space:
        self.xi1, self.eta1, self.xi2, self.eta2, self.xi3, self.eta3, self.xi4, self.eta4, self.xi5, self.eta5,\
            self.xi6, self.eta6, self.xi7, self.eta7, self.xi8, self.eta8 =\
            sp.symbols(r"\xi_1,\eta_1,\xi_2,\eta_2,\xi_3,\eta_3,\xi_4,\eta_4,\xi_5,\eta_5,\xi_6,\eta_6,\xi_7,\eta_7,\xi_8,\eta_8")
        self.x1, self.y1, self.x2, self.y2, self.x3, self.y3, self.x4, self.y4, self.x5, self.y5, self.x6, self.y6, \
            self.x7, self.y7, self.x8, self.y8 = \
            sp.symbols("x_1,y_1,x_2,y_2,x_3,y_3,x_4,y_4,x_5,y_5,x_6,y_6,x_7,y_7,x_8,y_8")
        
        if(self.latexOutput):
            self.latexOut.addSubsection('Nodes\' position')
            self.latexOut.addText('In parametric space:')
            self.latexOut.addMath(sp.latex(self.xi1) + ', ' + sp.latex(self.eta1) + ', ' + sp.latex(self.xi2) + ', ' + sp.latex(self.eta2) + ', ' +\
                                  sp.latex(self.xi3) + ', ' + sp.latex(self.eta3) + ', ' + sp.latex(self.xi4) + ', ' + sp.latex(self.eta4) + ', ' +\
                                      sp.latex(self.xi5) + ', ' + sp.latex(self.eta5) + ', ' + sp.latex(self.xi6) + ', ' + sp.latex(self.eta6) + ', ' + \
                                          sp.latex(self.xi7) + ', ' + sp.latex(self.eta7) + ', ' + sp.latex(self.xi8) + ', ' + sp.latex(self.eta8))
            self.latexOut.addText('In metric space:')
            self.latexOut.addMath(sp.latex(self.x1) + ', ' + sp.latex(self.y1) + ', ' + sp.latex(self.x2) + ', ' + sp.latex(self.y2) + ', ' + \
                                  sp.latex(self.x3) + ', ' + sp.latex(self.y3) + ', ' + sp.latex(self.x4) + ', ' + sp.latex(self.y4) + ', ' + \
                                      sp.latex(self.x5) + ', ' + sp.latex(self.y5) + ', ' + sp.latex(self.x6) + ', ' + sp.latex(self.y6) + ', ' +\
                                          sp.latex(self.x7) + ', ' + sp.latex(self.y7) + ', ' + sp.latex(self.x8) + ', ' + sp.latex(self.y8))
        return
    
    def calculateShapeFunction(self):
        if(self.isInitialised == False):
            self.initialiseElement()
            
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating shape functions.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Calculating shape functions.')
        
        X_1j = sp.Matrix([[1, self.xi, self.eta, self.xi**2, self.xi*self.eta, self.eta**2, self.eta*self.xi**2, \
                           self.xi*self.eta**2]])
        
        if(self.latexOutput):
            self.latexOut.addSubsection('Position/displacement basis function (based on isoparametric FE assumption)')
            self.latexOut.addMath(r'\left[X\right] = ' + sp.latex(X_1j))
        
        self.x_i = sp.Matrix([[self.x1],[self.y1],[self.x2],[self.y2],[self.x3],[self.y3],[self.x4],[self.y4], \
                              [self.x5],[self.y5],[self.x6],[self.y6],[self.x7],[self.y7],[self.x8],[self.y8]])
        
        if(self.latexOutput):
            self.latexOut.addSubsection('Vector of nodal coordinates (in metric space):')
            self.latexOut.addMath(r'\left\{x_N\right\} = ' + sp.latex(self.x_i))

        X_ij = sp.Matrix([X_1j.subs([(self.xi,self.xi1),(self.eta,self.eta1)]),
                           X_1j.subs([(self.xi,self.xi2),(self.eta,self.eta2)]),\
                               X_1j.subs([(self.xi,self.xi3),(self.eta,self.eta3)]),\
                               X_1j.subs([(self.xi,self.xi4),(self.eta,self.eta4)]), \
                                   X_1j.subs([(self.xi,self.xi5),(self.eta,self.eta5)]), \
                                       X_1j.subs([(self.xi,self.xi6),(self.eta,self.eta6)]), \
                                           X_1j.subs([(self.xi,self.xi7),(self.eta,self.eta7)]), \
                                               X_1j.subs([(self.xi,self.xi8),(self.eta,self.eta8)])])
        
        X_ij_subst = X_ij.subs([(self.xi1,self.nodesCoordinatesParametric[0, 0]),(self.eta1,self.nodesCoordinatesParametric[0, 1]),\
                                              (self.xi2,self.nodesCoordinatesParametric[1, 0]),(self.eta2,self.nodesCoordinatesParametric[1, 1]),\
                                                  (self.xi3,self.nodesCoordinatesParametric[2, 0]),(self.eta3,self.nodesCoordinatesParametric[2, 1]),\
                                                      (self.xi4,self.nodesCoordinatesParametric[3, 0]),(self.eta4,self.nodesCoordinatesParametric[3, 1]),\
                                                          (self.xi5,self.nodesCoordinatesParametric[4, 0]),(self.eta5,self.nodesCoordinatesParametric[4, 1]),\
                                                              (self.xi6,self.nodesCoordinatesParametric[5, 0]),(self.eta6,self.nodesCoordinatesParametric[5, 1]),\
                                                                  (self.xi7,self.nodesCoordinatesParametric[6, 0]),(self.eta7,self.nodesCoordinatesParametric[6, 1]),\
                                                                      (self.xi8,self.nodesCoordinatesParametric[7, 0]),(self.eta8,self.nodesCoordinatesParametric[7, 1])])
            
        if(self.latexOutput):
            self.latexOut.addSubsection('Matrix of constants:')
            self.latexOut.addMath(r'\left[C\right] = ' + sp.latex(X_ij) + ' = ' + sp.latex(X_ij_subst))
        
        self.shapeFunction = X_1j*X_ij_subst**-1
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Shape functions calculated.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Shape functions calculated.')
        
        if(self.latexOutput):
            self.latexOut.addSubsection('Displacement shape functions:')
            self.latexOut.addMath(r'\left[N\right] = ' + sp.latex(self.shapeFunction))
            
        return
    
    def get_element_stiff_matrix(self, nodesCoordinatesList, eldata):
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' +\
                                 'Importing coordinates of the nodes, Young\'s modulus and Poisson\'s coefficient from an external program. Calculating stiffness matrix evaluated with the imported data.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Importing data from an external program and evaluating stiffness matrix.')
        
        self.EeNum = eldata[0]
        self.nuNum = eldata[1]
        self.tNum = eldata[2]
        
        if(self.name == 'PlnStrssQd2Red1Rot2'):
            self.nodesCoordinatesNum[0] = nodesCoordinatesList[0]
            self.nodesCoordinatesNum[1, 0] = (nodesCoordinatesList[0][0] + nodesCoordinatesList[1][0])/2
            self.nodesCoordinatesNum[1, 1] = (nodesCoordinatesList[0][1] + nodesCoordinatesList[1][1])/2
            self.nodesCoordinatesNum[2] = nodesCoordinatesList[1]
            self.nodesCoordinatesNum[3, 0] = (nodesCoordinatesList[1][0] + nodesCoordinatesList[2][0])/2
            self.nodesCoordinatesNum[3, 1] = (nodesCoordinatesList[1][1] + nodesCoordinatesList[2][1])/2
            self.nodesCoordinatesNum[4] = nodesCoordinatesList[2]
            self.nodesCoordinatesNum[5, 0] = (nodesCoordinatesList[2][0] + nodesCoordinatesList[3][0])/2
            self.nodesCoordinatesNum[5, 1] = (nodesCoordinatesList[2][1] + nodesCoordinatesList[3][1])/2
            self.nodesCoordinatesNum[6] = nodesCoordinatesList[3]
            self.nodesCoordinatesNum[7, 0] = (nodesCoordinatesList[3][0] + nodesCoordinatesList[0][0])/2
            self.nodesCoordinatesNum[7, 1] = (nodesCoordinatesList[3][1] + nodesCoordinatesList[0][1])/2
        else:
            self.nodesCoordinatesNum[0] = nodesCoordinatesList[0]
            self.nodesCoordinatesNum[1] = nodesCoordinatesList[1]
            self.nodesCoordinatesNum[2] = nodesCoordinatesList[2]
            self.nodesCoordinatesNum[3] = nodesCoordinatesList[3]
            self.nodesCoordinatesNum[4] = nodesCoordinatesList[4]
            self.nodesCoordinatesNum[5] = nodesCoordinatesList[5]
            self.nodesCoordinatesNum[6] = nodesCoordinatesList[6]
            self.nodesCoordinatesNum[7] = nodesCoordinatesList[7]
        """
        if(self.isMatrixEmpty(self.stiffnessMatrix)):
            self.calculateStiffnessMatrix()
        
        matTemp = self.stiffnessMatrix.subs([(self.Ee, self.EeNum), (self.nu, self.nuNum), (self.t, self.tNum),\
                                             (self.x1, self.nodesCoordinatesNum[0, 0]), (self.y1, self.nodesCoordinatesNum[0, 1]),\
                                                 (self.x2, self.nodesCoordinatesNum[1, 0]), (self.y2, self.nodesCoordinatesNum[1, 1]),\
                                                     (self.x3, self.nodesCoordinatesNum[2, 0]), (self.y3, self.nodesCoordinatesNum[2, 1]),\
                                                         (self.x4, self.nodesCoordinatesNum[3, 0]), (self.y4, self.nodesCoordinatesNum[3, 1]),\
                                                             (self.x5, self.nodesCoordinatesNum[4, 0]), (self.y5, self.nodesCoordinatesNum[4, 1]),\
                                                                 (self.x6, self.nodesCoordinatesNum[5, 0]), (self.y6, self.nodesCoordinatesNum[5, 1]),\
                                                                     (self.x7, self.nodesCoordinatesNum[6, 0]), (self.y7, self.nodesCoordinatesNum[6, 1]),\
                                                                         (self.x8, self.nodesCoordinatesNum[7, 0]), (self.y8, self.nodesCoordinatesNum[7, 1])])
        
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' +\
                                 'Evaluating stiffness matrix for current element.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Evaluating stiffness matrix for current element.')
            
        matTempEvaluated = matTemp.evalf()
        self.stiffnessMatrixNum = np.array(matTempEvaluated.tolist()).astype(np.float64)
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix evaluated at nodes given by an external code together with Young\'s modulus and Poisson\'s coefficient. Stiffness matrix handed over to the external code.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix evaluated as required by an external code and handed over to it.')
        """
        
        if(self.isMatrixEmpty(self.BMatrix)):
            self.calculateBMatrix()
            self.getDMatrix()
        elif(self.isMatrixEmpty(self.jacobianInverse)):
            self.calculateJacobianItsInverseAndDeterminant()
            
        if(self.saveAnalysisDetails and self.analysisDetailsMode == 'D'):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' +\
                                 'Evaluating stiffness matrix for current element.\n')
        if(self.printAnalysisDetails and self.analysisDetailsMode == 'D'):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Evaluating stiffness matrix for current element.')
        
        matTemp = self.BMatrix.subs([(self.Ee, self.EeNum), (self.nu, self.nuNum), (self.t, self.tNum),\
                                             (self.x1, self.nodesCoordinatesNum[0, 0]), (self.y1, self.nodesCoordinatesNum[0, 1]),\
                                                 (self.x2, self.nodesCoordinatesNum[1, 0]), (self.y2, self.nodesCoordinatesNum[1, 1]),\
                                                     (self.x3, self.nodesCoordinatesNum[2, 0]), (self.y3, self.nodesCoordinatesNum[2, 1]),\
                                                         (self.x4, self.nodesCoordinatesNum[3, 0]), (self.y4, self.nodesCoordinatesNum[3, 1]),\
                                                             (self.x5, self.nodesCoordinatesNum[4, 0]), (self.y5, self.nodesCoordinatesNum[4, 1]),\
                                                                 (self.x6, self.nodesCoordinatesNum[5, 0]), (self.y6, self.nodesCoordinatesNum[5, 1]),\
                                                                     (self.x7, self.nodesCoordinatesNum[6, 0]), (self.y7, self.nodesCoordinatesNum[6, 1]),\
                                                                         (self.x8, self.nodesCoordinatesNum[7, 0]), (self.y8, self.nodesCoordinatesNum[7, 1])])
        BMatEvaluated = matTemp.evalf()
        
        matTemp = self.plnStrssD.subs([(self.Ee, self.EeNum), (self.nu, self.nuNum)])
        DMatEvaluated = matTemp.evalf()
        
        matTemp = self.jacobianDeterminant.subs([(self.Ee, self.EeNum), (self.nu, self.nuNum), (self.t, self.tNum),\
                                             (self.x1, self.nodesCoordinatesNum[0, 0]), (self.y1, self.nodesCoordinatesNum[0, 1]),\
                                                 (self.x2, self.nodesCoordinatesNum[1, 0]), (self.y2, self.nodesCoordinatesNum[1, 1]),\
                                                     (self.x3, self.nodesCoordinatesNum[2, 0]), (self.y3, self.nodesCoordinatesNum[2, 1]),\
                                                         (self.x4, self.nodesCoordinatesNum[3, 0]), (self.y4, self.nodesCoordinatesNum[3, 1]),\
                                                             (self.x5, self.nodesCoordinatesNum[4, 0]), (self.y5, self.nodesCoordinatesNum[4, 1]),\
                                                                 (self.x6, self.nodesCoordinatesNum[5, 0]), (self.y6, self.nodesCoordinatesNum[5, 1]),\
                                                                     (self.x7, self.nodesCoordinatesNum[6, 0]), (self.y7, self.nodesCoordinatesNum[6, 1]),\
                                                                         (self.x8, self.nodesCoordinatesNum[7, 0]), (self.y8, self.nodesCoordinatesNum[7, 1])])
        jacDetEvaluated = matTemp.evalf()
        
        TP = BMatEvaluated.T*DMatEvaluated*BMatEvaluated
        
        stiffnessMatrix = self.t.subs(self.t, self.tNum)*(jacDetEvaluated.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])*TP.subs([(self.xi,self.gp[0, 0]),(self.eta,self.gp[0, 1])])+\
                                                                jacDetEvaluated.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])*TP.subs([(self.xi,self.gp[1, 0]),(self.eta,self.gp[1, 1])])+\
                                                                    jacDetEvaluated.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])*TP.subs([(self.xi,self.gp[2, 0]),(self.eta,self.gp[2, 1])])+\
                                                                    jacDetEvaluated.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])])*TP.subs([(self.xi,self.gp[3, 0]),(self.eta,self.gp[3, 1])]))
        matTempEvaluated = stiffnessMatrix.evalf()
        self.stiffnessMatrixNum = np.array(matTempEvaluated.tolist()).astype(np.float64)
        
        if(self.saveAnalysisDetails):
            with open(self.analysisDetailsFileName, 'a') as outputFile:
                outputFile.write(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix evaluated at nodes given by an external code together with Young\'s modulus and Poisson\'s coefficient. Stiffness matrix handed over to the external code.\n')
        if(self.printAnalysisDetails):
            print(dt.datetime.now().strftime('%x %X') + '\t' + 'Stiffness matrix evaluated as required by an external code and handed over to it.')
        
        return self.stiffnessMatrixNum
    
    def get_node_dofs(self):
        return fes.getFE(self.name)